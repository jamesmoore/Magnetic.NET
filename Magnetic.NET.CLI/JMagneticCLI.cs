﻿using Magnetic.NET.Graphics;
using Magnetic.NET.Sound;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Magnetic.NET.CLI
{
    class JMagneticCLI : JMagneticCore
    {
        private ILogger logger;
        private IMagneticGraphics magneticGraphics = null;

        public JMagneticCLI(ILogger logger,
            IMagneticGraphics magneticGraphics
            )
            : base(logger,
               new MagneticIOCLI()
            )
        {
            this.logger = logger;
            this.magneticGraphics = magneticGraphics;
        }

        public override byte ms_load_file(char[] name, byte[] loadBuffer)
        {
            string loadFileName = null;
            if (name == null)
            {
                loadFileName = this.GetLoadFilename();
            }
            else
            {
                loadFileName = new string(name);
            }

            if (loadFileName != null)
            {
                var loadBytes = File.ReadAllBytes(loadFileName);
                Array.Copy(loadBytes, 0, loadBuffer, 0, loadBuffer.Length);
                return 0;
            }
            else
            {
                return 1;
            }

        }

        private string GetLoadFilename()
        {
            using (var sFile = new OpenFileDialog())
            {
                var saveFile = sFile.ShowDialog();
                if (saveFile == DialogResult.OK)
                {
                    var filename = sFile.FileName;
                    return filename;
                }
                else
                {
                    return null;
                }
            }
        }

        public override byte ms_save_file(char[] name, byte[] saveArray)
        {
            string saveFileName = null;
            if (name == null)
            {
                saveFileName = this.GetSaveFilename();
            }
            else
            {
                saveFileName = new string(name);
            }

            if (saveFileName != null)
            {
                File.WriteAllBytes(saveFileName, saveArray);
                return 0;
            }
            else
            {
                return 1;
            }
        }

        private string GetSaveFilename()
        {
            using (var sFile = new SaveFileDialog())
            {
                var saveFile = sFile.ShowDialog();
                if (saveFile == DialogResult.OK)
                {
                    var filename = sFile.FileName;
                    return filename;
                }
                else
                {
                    return null;
                }
            }
        }

        private Task formTask;
        private PictureForm f;


        public override void ms_showpic(int c, string picName, byte mode)
        {
            var sw = new Stopwatch();
            sw.Start();

            var modeString = mode == 0 ? "Graphics Off" : mode == 1 ? "Thumbnails" : "Normal";
            var debugInfo = "ms_showpic " + c + " " + modeString;
            var picture = this.magneticGraphics.ms_extract(c, picName);
            if (picture == null) return;

            if (f == null)
            {
                f = new PictureForm();
                f.FormClosed += f_FormClosed;
            }

            if (formTask == null || formTask.Status != TaskStatus.Running && formTask.Status != TaskStatus.WaitingToRun)
            {
                formTask = new Task(() => { this.StartNewStaThread(); });
                formTask.Start();
            }

            debugInfo += " " + picture.CurrPWidth + "x" + picture.CurrPHeight;
            //if (this.main_pic != null)
            //{
            //    debugInfo += " animated: " + this.main_pic.isAnim;
            //    if (this.main_pic.isAnim)
            //    {
            //        debugInfo += " repeated: " + this.main_pic.anim_repeat;
            //        debugInfo += " frames: " + this.main_pic.animFrames.Count;
            //    }
            //}
            logger.debug(debugInfo);

            var rgbPalette = picture.CurrPalette.Select(p => CreateArgb((short)p)).ToArray();

            f.StopAnimation();
            if (tokenSource != null)
            {
                tokenSource.Cancel();
            }
            if (animationTask != null)
            {
                animationTask.Wait();
            }

            f.SetPicture(picture.CurrPWidth, picture.CurrPHeight, picture.gfx_buf, rgbPalette, true);

            sw.Stop();

            logger.debug("Show picture timespan: " + sw.ElapsedMilliseconds);

            tokenSource = new CancellationTokenSource();
            cancellationToken = tokenSource.Token;
            animationTask = new Task(() => this.DoAnimations(picture, rgbPalette), cancellationToken);
            animationTask.Start();
        }

        void f_FormClosed(object sender, FormClosedEventArgs e)
        {
            f = null;
        }

        private CancellationTokenSource tokenSource = new CancellationTokenSource();
        private Task animationTask = null;
        private CancellationToken cancellationToken;

        private async void DoAnimations(MagneticPicture background, int[] rgbPalette)
        {
            var sw = new Stopwatch();
            sw.Start();

            var backupBuf = new byte[background.gfx_buf.Length];
            Array.Copy(background.gfx_buf, backupBuf, background.gfx_buf.Length);

            var positions = new List<JMagneticAniPos>();
            var position = 0;
            logger.debug("Position:\tx\ty\tframe\twidth\theight\tmask");

            bool ms_animate_returnValue;
            while (cancellationToken.IsCancellationRequested == false && (ms_animate_returnValue = this.magneticGraphics.ms_animate(positions)))
            {
                position++;
                try
                {
                    await Task.Delay(100, cancellationToken);
                }
                catch (TaskCanceledException)
                {
                    break;
                }

                f.SetPicture(background.CurrPWidth, background.CurrPHeight, backupBuf, rgbPalette, false);

                foreach (var anipos in positions.OfType<JMagneticAniPos>().Where(p => p.number >= 0))
                {
                    var af = this.magneticGraphics.ms_get_anim_frame(anipos.number);
                    logger.debug("\t" + position + "\t" + anipos.x + "\t" + anipos.y + "\t" + anipos.number + "\t" + af.width + "\t" + af.height + "\t" + (af.MaskBytes != null) + "\t" + (bool)ms_animate_returnValue);

                    var frameBitmap = new sbyte[af.height * af.width];

                    for (var y = 0; y < af.height; y++)
                    {
                        for (var x = 0; x < af.width; x++)
                        {
                            var paintPixel = false;
                            if (af.MaskBytes != null)
                            {
                                int wBlock = (int)((double)x / (double)8);
                                int pOff = (int)((double)x % (double)8);
                                var block = af.MaskBytes[(y * af.MaskByteWidth) + wBlock];
                                var bitBlock = new BitArray(new byte[] { block });
                                if (block == 0x00)
                                {
                                    paintPixel = true;
                                }
                                else
                                {
                                    paintPixel = !bitBlock[7 - pOff];
                                }

                            }
                            else
                            {
                                paintPixel = true;
                            }

                            frameBitmap[y * af.width + x] = paintPixel ? (sbyte)af.FrameBytes[(y * af.width) + x] : (sbyte)-1;

                        }
                    }
                    f.SetFrame(anipos.x, anipos.y, af.width, af.height, frameBitmap, rgbPalette);
                }
                if (cancellationToken.IsCancellationRequested == false)
                {
                    f.ShowFrames();
                    await Task.Yield();
                }
            }
            sw.Stop();
            logger.debug("Timespan: " + sw.ElapsedMilliseconds);
            if (position > 0)
            {
                logger.debug("Timespan/frameset: " + sw.ElapsedMilliseconds / position);
            }
        }

        private static int CreateArgb(short pixed)
        {
            var argb = 0xFF000000 | (0x0F00 & pixed) << 13 | (0x00F0 & pixed) << 9 | (0x000F & pixed) << 5;
            return (int)argb;
        }

        private void StartNewStaThread()
        {
            Application.Run(f);
        }

        public override void ms_fatal(string txt)
        {
            Console.WriteLine("ms_fatal");
            Console.WriteLine(txt);
        }

        public override void ms_playmusic(JMagneticSound name)
        {
            var sound = name;
            
        }
    }



}
