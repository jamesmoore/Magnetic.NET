﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnetic.NET.CLI
{
    public class MagneticIOCLI : IMagneticIO
    {
        private StringBuilder outputbuffer = new StringBuilder();

        public void ms_statuschar(char c)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write(c);
            Console.ResetColor();
        }

        public void ms_putchar(char c)
        {
            this.outputbuffer.Append(c);
        }

        public void ms_flush()
        {
            if (this.outputbuffer.Length > 0)
            {
                var linewidth = Console.WindowWidth;

                var currentLineLength = 0;
                var position = 0;
                var lastWhiteSpace = 0;
                while (position < this.outputbuffer.Length)
                {
                    var currentChar = outputbuffer[position];
                    if (currentChar == '\n')
                    {
                        currentLineLength = 0;
                        lastWhiteSpace = 0;
                    }
                    else if (char.IsWhiteSpace(currentChar))
                    {
                        lastWhiteSpace = position;
                    }

                    if (currentLineLength >= linewidth && currentLineLength % linewidth == 0)
                    {
                        if (char.IsWhiteSpace(outputbuffer[position + 1]))
                        {
                            outputbuffer.Remove(position + 1, 1);
                        }
                        else if (lastWhiteSpace > 0)
                        {
                            outputbuffer[lastWhiteSpace] = '\n';
                            currentLineLength = position - lastWhiteSpace;
                            lastWhiteSpace = 0;
                        }
                        else
                        {
                            outputbuffer.Insert(position + 1, '\n');
                        }
                    }

                    position++;
                    currentLineLength++;
                }
                Console.Write(this.outputbuffer.ToString());
                this.outputbuffer.Clear();
            }
        }

        private Queue<char> inputBuffer = null;

        public char ms_getchar(int trans)
        {
            if (inputBuffer == null)
            {
                var input = Console.ReadLine();


                inputBuffer = new Queue<char>();

                if (input.ToLower() == "undo")
                {
                    inputBuffer.Enqueue('\0');
                }
                else
                {
                    foreach (var charInput in input)
                    {
                        inputBuffer.Enqueue(charInput);
                    }
                }


            }

            if (inputBuffer.Count == 0)
            {
                inputBuffer = null;
                return '\n';
            }
            else
            {
                return inputBuffer.Dequeue();
            }
        }


    }
}
