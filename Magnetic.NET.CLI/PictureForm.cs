﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Magnetic.NET.CLI
{
    class PictureForm : Form
    {
        public PictureForm()
        {
            this.Paint += PictureForm_Paint;
            this.DoubleBuffered = true;
            this.MouseWheel += PictureForm_MouseWheel;

        }

        void PictureForm_MouseWheel(object sender, MouseEventArgs e)
        {
            this.scalingFactor += e.Delta / Math.Abs(e.Delta);
            if (this.scalingFactor < 1) this.scalingFactor = 1;
            this.Invalidate();
        }

        object bmpLocker = new object();
        Bitmap bmp = null;
        Bitmap bmpWithFrames = null;

        void PictureForm_Paint(object sender, PaintEventArgs e)
        {
            lock (bmpLocker)
            {
                var bmpToDraw = bmpWithFrames ?? bmp;
                // potential race condition
                var graphics = e.Graphics;
                this.PaintBitmap(bmpToDraw, graphics);
            }
        }

        private int scalingFactor = 2;

        private void PaintBitmap(Bitmap bmpToDraw, System.Drawing.Graphics graphics)
        {
            if (bmpToDraw != null)
            {
                lock (bmpLocker)
                {
                    this.ClientSize = new Size((int)bmpToDraw.Width * scalingFactor, (int)bmpToDraw.Height * scalingFactor);

                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.SmoothingMode = SmoothingMode.HighQuality;
                    graphics.DrawImage(
                        bmpToDraw,
                        new Rectangle(0, 0, bmpToDraw.Width * scalingFactor, bmpToDraw.Height * scalingFactor),
                        0,
                        0,
                        bmpToDraw.Width,
                        bmpToDraw.Height,
                        GraphicsUnit.Pixel
                        );
                    graphics.Flush();
                }
            }
        }

        private class frame
        {
            public short x;
            public short y;
            public uint width;
            public uint height;
            public sbyte[] frameBitmap;
            public int[] rgbPalette;
        }

        private IList<frame> frameList = new List<frame>();

        internal void SetFrame(short x, short y, uint width, uint height, sbyte[] frameBitmap, int[] rgbPalette)
        {
            this.frameList.Add(new frame()
            {
                frameBitmap = frameBitmap,
                height = height,
                width = width,
                x = x,
                y = y,
                rgbPalette = rgbPalette,
            });
        }

        public void StopAnimation()
        {
            lock (bmpLocker)
            {
                this.frameList.Clear();
                this.bmpWithFrames = null;
            }
        }

        internal void ShowFrames()
        {
            lock (bmpLocker)
            {
                bmpWithFrames = (Bitmap)bmp.Clone();
                var maxHeight = bmp.Height;
                var maxWidth = bmp.Width;

                foreach (var frame in this.frameList)
                {
                    var frameHeight = frame.height;
                    var frameWidth = frame.width;
                    var frameBitmap = frame.frameBitmap;
                    var frameOffsetx = frame.x;
                    var frameOffsety = frame.y;

                    var frameClipX = frameOffsetx > 0 ? (short)0 : -1 * frameOffsetx;
                    var frameClipY = frameOffsety > 0 ? (short)0 : -1 * frameOffsety;
                    var frameClipWidth = frameOffsetx + frameWidth > maxWidth ? (uint)maxWidth - frameOffsetx : frameWidth;
                    var frameClipHeight = frameOffsety + frameHeight > maxHeight ? (uint)maxHeight - frameOffsety : frameHeight;

                    var framebmp = new Bitmap((int)frameClipWidth, (int)frameClipHeight);

                    var clippedBmp = new int[frameClipWidth * frameClipHeight];

                    for (int i = frameClipY; i < frameClipHeight; i++)
                    {
                        for (int j = frameClipX; j < frameClipWidth; j++)
                        {
                            var pixed = frameBitmap[i * frameWidth + j];
                            var pixel = pixed >= 0 ? frame.rgbPalette[pixed] : 0;
                            var arrayx = j - frameClipX;
                            var arrayy = i - frameClipY;
                            clippedBmp[arrayy * frameClipWidth + arrayx] = pixel;
                        }
                    }

                    var imageBytes = clippedBmp.Select(p => BitConverter.GetBytes(p)).SelectMany(p => p).ToArray();
                    var imageData = framebmp.LockBits(new Rectangle(0, 0, framebmp.Width, framebmp.Height), ImageLockMode.WriteOnly, framebmp.PixelFormat);
                    IntPtr scan0 = imageData.Scan0;
                    Marshal.Copy(imageBytes, 0, scan0, imageBytes.Length);
                    framebmp.UnlockBits(imageData);

                    using (var g = System.Drawing.Graphics.FromImage(bmpWithFrames))
                    {
                        g.CompositingMode = CompositingMode.SourceOver;
                        var x = frameOffsetx < 0 ? 0 : frameOffsetx;
                        var y = frameOffsety < 0 ? 0 : frameOffsety;
                        g.DrawImage(framebmp, new Point(x, y));
                    }
                }
            }
            this.frameList.Clear();
            this.Invalidate();
        }

        internal void SetPicture(uint width, uint height, byte[] bytes, int[] rgbPalette, bool refresh)
        {
            lock (this.bmpLocker)
            {
                if (bmp != null)
                {
                    bmp.Dispose();
                }

                bmp = new Bitmap((int)width, (int)height, PixelFormat.Format32bppArgb);

                var imageBytes = bytes.Take((int)(width * height)).Select(p => BitConverter.GetBytes(rgbPalette[p])).SelectMany(p => p).ToArray();
                var imageData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, bmp.PixelFormat);
                IntPtr scan0 = imageData.Scan0;
                Marshal.Copy(imageBytes, 0, scan0, imageBytes.Length);
                bmp.UnlockBits(imageData);
                bmpWithFrames = null;

                if (refresh)
                {
                    this.Invalidate();

                }
            }
        }


    }
}
