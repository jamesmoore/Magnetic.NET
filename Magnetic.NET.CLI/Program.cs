﻿using Magnetic.NET;
using Magnetic.NET.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnetic.NET.CLI
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            ILogger logger = new Logger(DateTime.Now.ToString("s").Replace(":", "-") + ".txt");
            logger = new NullLogger();
            var magGraphics = new MagneticGraphics(logger);
            var mag = new JMagneticCLI(logger, magGraphics);

            var gameList = GameInfo.ScanForGame().ToList();
            for (int i = 0; i < gameList.Count; i++)
            {
                Console.WriteLine((i + 1) + ") " + gameList[i].ShortName);
            }

            var input = Console.ReadLine();
            int inputNo = 0;
            if (int.TryParse(input, out inputNo) && inputNo > 0 && inputNo <= gameList.Count)
            {
                var game = gameList[inputNo - 1];
                mag.ms_init(
                    game.MagFile,
                    game.HntFile,
                    game.SndFile
                    );
                magGraphics.InitGfx(game.GfxFile, mag.version, mag.v4_id);
                mag.run();
            }

        }
    }
}
