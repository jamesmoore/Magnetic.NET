﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnetic.NET.Test
{
    class CommandProvider : ICommandProvider
    {
        private Queue<IOHistory> commandQueue;

        public CommandProvider(IEnumerable<IOHistory> commandList)
        {
            this.commandQueue = new Queue<IOHistory>();

            foreach (var line in commandList)
            {
                this.commandQueue.Enqueue(line);
            }
        }

        public IOHistory NextCommand()
        {
            return this.commandQueue.Dequeue();
        }

    }

}
