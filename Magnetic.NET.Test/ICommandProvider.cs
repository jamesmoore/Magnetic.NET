﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnetic.NET.Test
{
    public interface ICommandProvider
    {
        IOHistory NextCommand();
    }
}
