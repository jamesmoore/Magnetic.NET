﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnetic.NET.Test
{
    [DebuggerDisplay("{Input}: {Output}")]
    public class IOHistory
    {
        public IOHistory()
        {
            this.Output = string.Empty;
            this.Status = string.Empty;
        }

        public string Input { get; set; }
        public string Output { get; set; }
        public string Status { get; set; }

        public static implicit operator IOHistory(string val)
        {
            return new IOHistory() { Input = val };
        }

        public string ExpectedOutput { get; set; }

        public bool IsValid
        {
            get
            {
                if (string.IsNullOrEmpty(this.ExpectedOutput))
                {
                    return true;
                }
                else
                {
                    var result = this.ReplaceBackspace(this.Output).ToLower().Contains(this.ExpectedOutput.ToLower());
                    return result;
                }
            }
        }

        private string ReplaceBackspace(string hasBackspace)
        {
            if (string.IsNullOrEmpty(hasBackspace))
                return hasBackspace;

            StringBuilder result = new StringBuilder(hasBackspace.Length);
            foreach (char c in hasBackspace)
            {
                if (c == '\b')
                {
                    if (result.Length > 0)
                        result.Length--;
                }
                else
                {
                    result.Append(c);
                }
            }
            return result.ToString();
        }
    }
}
