﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnetic.NET.Test
{
    public class IOHistoryList : List<IOHistory>
    {
        public void Add(string input)
        {
            this.Add(new IOHistory()
            {
                Input = input,
            });
        }
        public void Add(string input, string expected)
        {
            this.Add(new IOHistory()
            {
                Input = input,
                ExpectedOutput = expected,
            });
        }
    }
}
