﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Net;
using System.IO.Compression;
using System.Collections.Generic;
using System.Linq;
using Magnetic.NET.Graphics;

namespace Magnetic.NET.Test
{
    [TestClass]
    public class MagneticCoreTests
    {
        public TestContext TestContext { get; set; }

        [TestMethod]
        public void BasicCodeCoverageTest()
        {
            var logger = new NullLogger();
            var mg = new MagneticGraphics(logger);
            foreach (var magfile in GameInfo.ScanForGame())
            {
                var ismw = this.ismagWin(magfile.MagFile);
                var commandList = GetCommandList(magfile.MagFile, string.IsNullOrEmpty(magfile.HntFile) == false, ismw);

                var mag = new TestableMagneticCore(
                    logger,
                    new TestableMagneticIO(
                        new CommandProvider(
                            commandList
                        )
                        ),
                    mg
                    );
                mag.enableEmuLog(false);
                mag.enableGfxExtLog(true);
                mag.enableGfxLog(true);
                mag.enableHntLog(true);
                mag.enableSndLog(true);

                var isrunning = mag.ms_is_running();
                Assert.AreEqual(false, isrunning);

                mag.ms_init(
                    magfile.MagFile,
                    magfile.HntFile,
                    magfile.SndFile
                    );
                mg.InitGfx(magfile.GfxFile, mag.version, mag.v4_id);

                isrunning = mag.ms_is_running();
                Assert.AreEqual(true, isrunning);

                var count = mag.ms_count();
                Assert.AreEqual(0, count);

                var isMWFromVM = (bool)mag.ms_is_magwin();
                Assert.AreEqual(ismw, isMWFromVM);

                mag.run();

                isrunning = mag.ms_is_running();
                Assert.AreEqual(false, isrunning);

                count = mag.ms_count();
                Assert.IsTrue(count > 0);

                var output = commandList;

                this.TestContext.WriteLine(magfile.MagFile);
                foreach (var line in output)
                {
                    this.TestContext.WriteLine("Input\t" + line.Input);
                    this.TestContext.WriteLine("Status\t" + line.Status);
                    this.TestContext.WriteLine("Output\t" + line.Output);
                    this.TestContext.WriteLine("IsValid\t" + line.IsValid);
                    Assert.IsTrue(line.IsValid);
                }

                mag.log_status();
                mag.ms_status();
                this.TestContext.WriteLine($"Instruction count: {count}");

            }


        }

        private bool ismagWin(string magfile)
        {
            magfile = magfile.ToLower();
            if (magfile.EndsWith("cguild.mag")) return true;
            if (magfile.EndsWith("wonder.mag")) return true;
            if (magfile.EndsWith("ccorrupt.mag")) return true;
            if (magfile.EndsWith("cfish.mag")) return true;

            return false;
        }

        private IOHistoryList GetCommandList(string magfile, bool hasHintsFile, bool ismw)
        {

            var commandList = new IOHistoryList();
            commandList.Add(new IOHistory());
            commandList.Add("graphics on", "graphics on");

            if (magfile.ToLower().Contains("guild"))
            {
                commandList.Add("examine master thief", "The thief is a shifty character with an expressionless 'poker' face.");
                commandList.Add("jump to jetty", "The joints of this rickety old jetty are becoming rotten and warped with constant lapping of the river water. The small boat is moored here off the east end of the jetty; westward is a tract of uncultivated land and to the southwest lies a field of golden wheat.");
                commandList.Add("w", "Here, bramble bushes and weeds that constantly tug and tear at your ankles have run amok. This type of foliage continues to the west and north but becomes stronger to the southwest, forming a copse. To the east is the rickety old jetty and a wheatfield lies to the south. Dragging an extremely heavy trunk is an old frail man, wearing a white sash.");
                commandList.Add("help man", "A grateful smile appears on the old man's face. Your extra weight makes the trunk move quite easily and you soon arrive at the castle's drawbridge. The gatekeeper notices you, lowers the drawbridge and drags the trunk into the castle. The old man follows him, disappearing into the depths of a place he obviously knows well.");
                commandList.Add("in", "All visitors are brought to this entrance hall on arrival at the castle. The floor is covered with a vast rug, woven in traditional Kerovnian style, and a wooden staircase leads up to the top floor. There are also exits in all four walls.");
                commandList.Add("w", "The lounge has many comfortable chairs and a shagpile carpet which brings a refreshing spring to your step. A coal fire blazes away by the hearth, giving off a warm, soft light which makes you feel relaxed and easy. There are exits in the south and east walls. A black iron bucket stands close to the fire.");
                commandList.Add("s", "Once upon a time, this gallery held the finest collection of art in all Kerovnia. However, during these days of financial hardship, most of the classics have been sold and only a few paintings by obscure contemporary artists remain. An exit leads north into the lounge and an ornately carved archway provides an exit south.");
                commandList.Add("s", "The Kerovnian ladies retire here after banquets and other functions at the castle. The room is mainly decorated in a pale yellow colour but there is an antique red leather settee against the south wall that clashes violently. The only exit from here is back through the archway into the gallery.");
                commandList.Add("open cushion");
                commandList.Add("get note", "You have now got the note.");
                commandList.Add("go to music room", "The main attraction here in the music room is a highly polished grand piano, manufactured by the famous Tychkoff sisters from Krewside-upon-Nasty, a small fishing town in northern Kerovnia. There are two exits from here, south and west. A stool sits conveniently close to the piano.");
            }

            if (magfile.ToLower().Contains("guild") && magfile.ToLower().Contains("cguild") == false)
            {
                commandList.Add("hint", "Please enter hint :");
                commandList.Add("5n c6 ef pq x6 t6 i7 ag ho co ea pt xj cp ex p5 xv cf em pt xz ce ie am h7 tu iq as hh t1 iy an hi t5 en ag 5g");
                commandList.Add("hint", "Please enter hint :");
                commandList.Add("an y6 nf oq 76 56 rk mi 7i yg ny or 75 yi r5 i5", "Read it again.");
                commandList.Add("hint", "Please enter hint :");
                commandList.Add("tn op 71 y6 na ma gu 5f rm mf 70 x0", "Jump wbtt.");
            }

            if (magfile.ToLower().Contains("wonder"))
            {
                commandList.Add("e", "You are in a grove full of pear trees, their branches laden with fruit busy ripening in the summer's sun. Most of the pears are out of your reach but one particular branch is so weighed down with fruit that you might just be able to reach a solitary pear hanging invitingly just a few feet above the ground.");
                commandList.Add("get pear", "You have now got the pear.");
                commandList.Add("w");
                commandList.Add("sw", "You are walking down a picturesque hedge-lined country lane which appears to go nowhere in particular unless you happen to be going in a north-easterly direction in which case it is more than likely to take you back to the river bank. You wonder idly whether you could return the favour and take the lane somewhere instead. A small overgrown stile lies almost hidden in the hedge to your west while the lane continues winding to the south.");
                commandList.Add("w", "By the looks of it this field has been freshly ploughed, leaving plenty of succulent worms for hungry beaks. The stile to your east appears to be the only way in or out of the field which is surrounded on all sides by a thick and thistly bramble hedge.");
                commandList.Add("wait", "Time passes.");
                commandList.Add("wait", "Time passes.");
                commandList.Add("wait", "The rabbit runs west across the field leaving a trail of paw prints and squeezes into a hole that you hadn't noticed before.");
                commandList.Add("w", "Getting down on your hands and knees, you manage to crawl inside the hole. All of a sudden, the lantern begins to glow just in time for you to see that the ceiling lowers sharply. You lower your head to avoid banging it.");
                commandList.Add("s");
                commandList.Add("s");
                commandList.Add("get jar");
                commandList.Add("wait");
                commandList.Add("w");
                commandList.Add("nw", "grand piano playing music all by itself");
            }

            if (magfile.ToLower().Contains("fish"))
            {
                commandList.Add("turn over", "The ant eggs have stopped swirling around and are now floating on the surface. Gravel fills the bottom of the bowl and some tasty looking pondweed you've been wanting to nibble for days is lying on it. You get a nasty feeling in your swim bladder about the castle, probably because Sir Playfair has so rudely interrupted your vacation.");
                commandList.Add("in", "Inside, the castle is exactly what you expected - tacky and featureless with ant eggs near the top and gravel at the bottom. To anyone else, the colours sprayed randomly on the wall would just be colours sprayed randomly on the wall. To a daring inter-dimensional espionage operative like yourself, however, they are obviously used to focus communications across the dimensions. The only exit is out, back to the goldfish bowl.");
                commandList.Add("in");
                commandList.Add("look in bin", "The waste bin contains a ferric oxide cassette.");
                commandList.Add("get cassette");
                commandList.Add("w");
                commandList.Add("open door", "Which one, the blue door or the kitchen door?");
                commandList.Add("blue");
                commandList.Add("w");
                commandList.Add("open door", "Which one, the wooden door or the secondary control room door?");
                commandList.Add("secondary control room");
                commandList.Add("n", "This room is used to edit tracks so terrible");
            }

            if (magfile.ToLower().Contains("pawn"))
            {
                commandList.Add("n", "You are on the gravel path leading north to the mountains. Northwestward, off the path, is an area of moorland just north of the forest. To the east is an extensive, grassy wilderness.");
                commandList.Add("n", "You are on the gravel path. Directly north are foothills leading up to the southernmost mountain. Westward is the mountainous region and the expanse of grassy plain is to the east.");
                commandList.Add("n", "You find yourself in the foothills below the nearest of the snow-capped mountains which looms up ominously in front of you. Around you, the floor is strewn with massive boulders. The only exits are south, along the gravel path, and a track leading northwest up the side of the mountain. Which is blocked by a particularly large boulder.");
                commandList.Add("l at boulder");
            }

            if (magfile.ToLower().Contains("corrupt"))
            {
                commandList.Add("l at margaret", "You can't see Margaret here.");
                if (ismw)
                {
                    commandList.Add("go to bathroom", "The plumbing in this building dates from the turn of the century - easy to believe with the smell coming from the cubicle to the south. The sink against the north wall gurgles at inopportune moments in time with the urinal. A mirror is hanging on the wall above the basin.");
                }
                else
                {
                    commandList.Add("go to bathroom", "You can't see a bathroom here.");
                }
            }

            if (hasHintsFile)
            {
                commandList.Add("hint");
                commandList.Add("1");
                commandList.Add("e");
            }

            commandList.Add("look");
            commandList.Add("exits");
            commandList.Add("look");


            commandList.Add("save");
            if (ismw == false)
            {
                commandList.Add("test.sav");
                commandList.Add("y");
            }
            commandList.Add("look");
            commandList.Add("score");
            commandList.Add("load");
            if (ismw == false)
            {
                commandList.Add("test.sav");
                commandList.Add("y");
            }
            commandList.Add("look at me");
            commandList.Add("i");
            commandList.Add("n");
            commandList.Add("s");
            commandList.Add("w");
            commandList.Add("e");
            commandList.Add(new string(new char[] { '\0' }));
            commandList.Add("quit", "restart");
            commandList.Add("r", "are you sure");
            commandList.Add("y");

            commandList.Add(new string(new char[] { '\0' }));
            commandList.Add("quit");
            commandList.Add("q", "are you sure");
            commandList.Add("y");
            return commandList;
        }

    }
}
