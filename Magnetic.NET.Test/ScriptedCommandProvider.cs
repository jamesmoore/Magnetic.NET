﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnetic.NET.Test
{
    public class ScriptedCommandProvider : ICommandProvider
    {
        private Queue<IOHistory> queue;

        public JMagneticCore core { get; set; }

        public ScriptedCommandProvider(IEnumerable<IOHistory> commandList)
        {
            this.queue = new Queue<IOHistory>();

            foreach (var line in commandList)
            {
                this.queue.Enqueue(line);
            }
        }

        public IOHistory NextCommand()
        {
            var next = queue.Dequeue();
            if(next.Input == "#seed")
            {
                next = queue.Dequeue();
                var rseed = int.Parse(next.Input);
                core.ms_seed(rseed);
                return new IOHistory()
                {
                    Input = ""
                };
            }
            else
            {
                return next;
            }

        }

        //private static List<IOHistory> ReadScript(string scriptFile)
        //{
        //    var items = File.ReadAllLines(scriptFile);

        //    var commandList = items.Select(p => new IOHistory() { Input = p }).ToList();

        //    commandList.Add(new IOHistory() { Input = "quit" });
        //    commandList.Add(new IOHistory() { Input = "q", ExpectedOutput = "are you sure" });
        //    commandList.Add(new IOHistory() { Input = "y" });

        //    commandList.Insert(0, new IOHistory());
        //    return commandList;
        //}
    }
}
