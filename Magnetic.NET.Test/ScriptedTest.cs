﻿using Magnetic.NET.Graphics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Magnetic.NET.Test
{
    [TestClass]
    public class ScriptedTest
    {

        public TestContext TestContext { get; set; }

        public void FullScriptedTest(string name, string scriptFile, IEnumerable<string> commands)
        {
            var assemblyLocation = Assembly.GetExecutingAssembly().Location;
            this.TestContext.WriteLine($"Executing in path {assemblyLocation}");
            var path = Path.GetDirectoryName(assemblyLocation);
            var logger = new NullLogger();
            var magfile = GameInfo.ScanForGame(path).FirstOrDefault(p => p.ShortName == name);
            if (magfile == null)
            {
                throw new InvalidOperationException($"No mag file in path for {name} in {path} for assembly {assemblyLocation}");
            }
            RunScriptForGame(logger, magfile, scriptFile, commands);
        }

        private void FullScriptedTest(string name, byte[] bytes)
        {
            var script = Encoding.UTF8.GetString(bytes);

            var lines = script.Split(
                new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.RemoveEmptyEntries
            );
            FullScriptedTest(name, name, lines);
        }

        [TestMethod]
        public void TestPawn()
        {
            FullScriptedTest("pawn", Scripts.Pawn);
        }

        [TestMethod]
        public void TestGuild()
        {
            FullScriptedTest("guild", Scripts.Guild);
        }
        [TestMethod]
        public void TestJinxter()
        {
            FullScriptedTest("jinxter", Scripts.Jinxter);
        }
        [TestMethod]
        public void TestCorruption()
        {
            FullScriptedTest("corrupt", Scripts.Corrupt);
        }
        [TestMethod]
        public void TestFish()
        {
            FullScriptedTest("fish", Scripts.Fish);
        }
        [TestMethod]
        public void TestMyth()
        {
            FullScriptedTest("myth", Scripts.Myth);
        }
        [TestMethod]
        public void TestWonder()
        {
            FullScriptedTest("wonder", Scripts.Wonder);
        }
        [TestMethod]
        public void TestCCorrupt()
        {
            FullScriptedTest("ccorrupt", Scripts.Corrupt);
        }
        [TestMethod]
        public void TestCGuild()
        {
            FullScriptedTest("cguild", Scripts.GuildColl);
        }
        [TestMethod]
        public void TestCFish()
        {
            FullScriptedTest("cfish", Scripts.FishColl);
        }

        private void RunScriptForGame(NullLogger logger, GameInfo magfile, string scriptFile, IEnumerable<string> commands)
        {
            var commandList = ReadScript(scriptFile, commands);
            RunScriptForGame(logger, magfile, commandList);
        }

        private void RunScriptForGame(NullLogger logger, GameInfo magfile, List<IOHistory> commandList)
        {
            var mg = new MagneticGraphics(logger);
            var scriptedCommandProvider = new ScriptedCommandProvider(commandList);

            var mag = new TestableMagneticCore(
                logger,
                new TestableMagneticIO(scriptedCommandProvider),
                mg
                );
            scriptedCommandProvider.core = mag;

            mag.enableEmuLog(false);
            mag.enableGfxExtLog(true);
            mag.enableGfxLog(true);
            mag.enableHntLog(true);
            mag.enableSndLog(true);

            var isrunning = mag.ms_is_running();
            Assert.AreEqual(false, isrunning);

            mag.ms_init(
                magfile.MagFile,
                magfile.HntFile,
                magfile.SndFile
                );
            mg.InitGfx(magfile.GfxFile, mag.version, mag.v4_id);
            isrunning = mag.ms_is_running();
            Assert.AreEqual(true, isrunning);

            var count = mag.ms_count();
            Assert.AreEqual(0, count);

            mag.run();

            isrunning = mag.ms_is_running();
            Assert.AreEqual(false, isrunning);

            count = mag.ms_count();
            Assert.IsTrue(count > 0);

            this.TestContext.WriteLine(magfile.MagFile);
            foreach (var line in commandList)
            {
                this.TestContext.WriteLine("Input\t" + line.Input);
                this.TestContext.WriteLine("Status\t" + line.Status);
                this.TestContext.WriteLine("Output\t" + line.Output);
                this.TestContext.WriteLine("IsValid\t" + line.IsValid);
                Assert.IsTrue(line.IsValid);
            }

            mag.log_status();
            mag.ms_status();
            this.TestContext.WriteLine($"Instruction count: {count}");
        }

        private static List<IOHistory> ReadScript(string scriptFile, IEnumerable<string> commands)
        {
            var commandList = commands.Select(p => new IOHistory() { Input = p }).ToList();

            if (scriptFile.ToLower().Contains("pawn"))
            {
                commandList.Add(new IOHistory() { Input = "quit" });
            }
            commandList.Add(new IOHistory() { Input = "q", ExpectedOutput = "are you sure" });
            commandList.Add(new IOHistory() { Input = "y" });
            commandList.Add(new IOHistory() { Input = "" });
            commandList.Insert(0, new IOHistory());
            return commandList;
        }
    }
}
