﻿using Magnetic.NET.Graphics;
using Magnetic.NET.Sound;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnetic.NET.Test
{
    public class TestableMagneticCore : JMagneticCore
    {
        private ITestableMagneticIO magneticIO;
        private IMagneticGraphics magneticGraphics = null;
        public TestableMagneticCore(
            ILogger logger,
            ITestableMagneticIO magneticIO,
            IMagneticGraphics magneticGraphics
            )
            : base(logger, magneticIO)
        {
            this.magneticIO = magneticIO;
            this.magneticGraphics = magneticGraphics;
        }

        //public IList<IOHistory> GetOutputList()
        //{
        //    return this.historyList;
        //}

        private byte[] saveArray = null;

        public override byte ms_load_file(char[] name, byte[] loadBuffer)
        {
            if (this.ms_is_magwin())
            {
                Assert.IsNull(name);
            }
            else
            {
                Assert.IsNotNull(name);
            }


            if (this.saveArray == null || this.saveArray.Length == 0)
            {
                throw new ApplicationException("no save data");
            }
            Array.Copy(this.saveArray, 0, loadBuffer, 0, loadBuffer.Length);
            return 0;
        }

        public override byte ms_save_file(char[] name, byte[] saveArray)
        {
            if (this.ms_is_magwin())
            {
                Assert.IsNull(name);
            }
            else
            {
                Assert.IsNotNull(name);
            }

            this.saveArray = saveArray;
            return 0;
        }



        public override void ms_showpic(int c, string picName, byte mode)
        {
            var picture = this.magneticGraphics.ms_extract(c, picName);
            if (picture != null)
            {
                if (c != 0 && picture.CurrPWidth == 0) throw new ApplicationException("Zero width picture");
                if (c != 0 && picture.CurrPHeight == 0) throw new ApplicationException("Zero height picture");

                var positions = new List<JMagneticAniPos>();

                bool ms_animate_returnValue;
                while ((ms_animate_returnValue = this.magneticGraphics.ms_animate(positions)))
                {
                    foreach (var position in positions.OfType<JMagneticAniPos>())
                    {
                        var frameProps = this.magneticGraphics.ms_get_anim_frame(position.number);
                    }
                    break; // Exit after one loop for tests
                }
            }
        }

        public override void ms_fatal(string txt)
        {
            if (this.magneticIO.HasRemaingInput)
            {
                throw new ApplicationException("fatal exception before end of input");
            }
        }

        public override void ms_playmusic(JMagneticSound name)
        {

        }
    }

}
