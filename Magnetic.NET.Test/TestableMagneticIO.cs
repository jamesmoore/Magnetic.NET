﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnetic.NET.Test
{

    class TestableMagneticIO : ITestableMagneticIO
    {
        private IOHistory currentIO;
        private ICommandProvider commandProvider;
        private StringBuilder statusBuffer = new StringBuilder();
        private Queue<char> inputCharQueue = new Queue<char>();
        private StringBuilder outputbuffer = new StringBuilder();

        public TestableMagneticIO(ICommandProvider commandProvider)
        {
            this.commandProvider = commandProvider;
            this.currentIO = this.commandProvider.NextCommand();
        }


        public void ms_statuschar(char c)
        {
            this.statusBuffer.Append(c);
        }

        public void ms_putchar(char c)
        {
            outputbuffer.Append(c);
        }

        public void ms_flush()
        {
            if (outputbuffer.Length > 0)
            {
                this.currentIO.Output += outputbuffer.ToString();
                this.outputbuffer.Clear();
            }
            if (statusBuffer.Length > 0)
            {
                Debug.Assert(string.IsNullOrEmpty(this.currentIO.Status));
                this.currentIO.Status += statusBuffer.ToString();
                this.statusBuffer.Clear();
            }
        }

        public char ms_getchar(int trans)
        {
            if (this.inputCharQueue.Any() == false)
            {
                this.currentIO = this.commandProvider.NextCommand();
                if (currentIO != null)
                {
                    foreach (var lineChar in currentIO.Input)
                    {
                        this.inputCharQueue.Enqueue(lineChar);
                    }
                    inputCharQueue.Enqueue('\n');
                }
                else
                {
                    throw new ApplicationException("No line");
                }
            }

            var ch = this.inputCharQueue.Dequeue();
            return ch;
        }

        public bool HasRemaingInput
        {
            get
            {
                return this.inputCharQueue.Any();
            }
        }
    }
}
