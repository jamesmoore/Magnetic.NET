﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices;
using System.Web.Http;

namespace Magnetic.NET.WebUI.Controllers
{
    public class PictureController : ApiController
    {
        public HttpResponseMessage GetPicture(string name, int id)
        {
            var picture = MagneticClient.GetPictureFromStorage(name, id);

            var width = picture.CurrPWidth;
            var height = picture.CurrPHeight;
            var bytes = picture.gfx_buf;
            var rgbPalette = picture.CurrPalette.Select(p => CreateArgb((short)p)).ToArray();

            var bmp = new Bitmap((int)width, (int)height, PixelFormat.Format32bppArgb);

            var imageBytes = bytes.Take((int)(width * height)).Select(p => BitConverter.GetBytes(rgbPalette[p])).SelectMany(p => p).ToArray();
            var imageData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, bmp.PixelFormat);
            IntPtr scan0 = imageData.Scan0;
            Marshal.Copy(imageBytes, 0, scan0, imageBytes.Length);
            bmp.UnlockBits(imageData);

            var ms = new MemoryStream();
            bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            var result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new ByteArrayContent(ms.ToArray());
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("image/png");
            return result;

            //var result = new HttpResponseMessage(HttpStatusCode.OK);
            //result.Content = new StringContent(name + " : " + (id * id).ToString() + " " + (picture != null));
            //return result;
        }

        private static int CreateArgb(short pixed)
        {
            var argb = 0xFF000000 | (0x0F00 & pixed) << 13 | (0x00F0 & pixed) << 9 | (0x000F & pixed) << 5;
            return (int)argb;
        }
    }
}
