﻿using Magnetic.NET.Graphics;
using Magnetic.NET.Sound;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;

namespace Magnetic.NET.WebUI
{
    public class MagneticClient : JMagneticCore
    {
        private ILogger logger;
        private MagneticGameHub hub;
        private string gameId;
        private static Dictionary<string, Dictionary<int, MagneticPicture>> pictureStorage = new Dictionary<string, Dictionary<int, MagneticPicture>>();
        public MagneticIO MagneticIO { get; private set; }
        public MagneticGraphics magneticGraphics;
        public MagneticClient(
            ILogger logger,
            MagneticGameHub hub,
            string gameId,
            MagneticIO magneticIO,
            MagneticGraphics mg
            )
            : base(logger, magneticIO)
        {
            this.logger = logger;
            this.hub = hub;
            this.gameId = gameId;
            this.MagneticIO = magneticIO;
            this.magneticGraphics = mg;
        }

        public override byte ms_load_file(char[] name, byte[] loadBuffer)
        {
            throw new NotImplementedException();
        }

        public override byte ms_save_file(char[] name, byte[] saveArray)
        {
            throw new NotImplementedException();
        }

        public override void ms_showpic(int c, string picName, byte mode)
        {
            if (IsPictureInStorage(this.gameId, c) == false)
            {
                var picture = this.magneticGraphics.ms_extract(c, picName);
                AddPictureToStorage(this.gameId, c, picture);
            }
            hub.ShowPicture(this.gameId, c, mode);
        }

        public static MagneticPicture GetPictureFromStorage(string id, int c)
        {
            return pictureStorage[id][c];
        }

        private static bool IsPictureInStorage(string id, int c)
        {
            if (pictureStorage.ContainsKey(id))
            {
                if (pictureStorage[id].ContainsKey(c))
                {
                    return true;
                }
            }
            return false;
        }

        private static void AddPictureToStorage(string id, int c, MagneticPicture picture)
        {
            lock (pictureStorage)
            {
                if (pictureStorage.ContainsKey(id) == false)
                {
                    pictureStorage.Add(id, new Dictionary<int, MagneticPicture>());
                }

                var perGamePictureStorage = pictureStorage[id];
                lock (perGamePictureStorage)
                {
                    if (perGamePictureStorage.ContainsKey(c) == false)
                    {
                        perGamePictureStorage.Add(c, picture);
                    }
                }
            }
        }


        public override void ms_fatal(string txt)
        {
        }

        public override void ms_playmusic(JMagneticSound name)
        {
        }
    }
}