﻿using Magnetic.NET.Graphics;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;

namespace Magnetic.NET.WebUI
{
    public class MagneticGameHub : Hub
    {
        public IEnumerable<GameInfo> ScanForGame()
        {
            return GetGames();
        }

        private static IEnumerable<GameInfo> GetGames()
        {
            var directory0 = HttpRuntime.AppDomainAppPath;
            var gameList = GameInfo.ScanForGame(directory0);
            return gameList;
        }

        public void Send(string message)
        {
            var connectionId = Context.ConnectionId;

            if (message.ToLower().StartsWith("play"))
            {
                var afterPlay = message.Substring(4).Trim().ToLower();
                StartGame(afterPlay);
            }
            else if (gameInstances.ContainsKey(connectionId))
            {
                gameInstances[connectionId].MagneticIO.SetInput(message);
            }
            else
            {
            }
        }

        public void StartGame(string afterPlay)
        {
            var connectionId2 = Context.ConnectionId;
            var games = GetGames();
            var game = games.FirstOrDefault(p => p.ShortName.ToLower() == afterPlay);
            if (game != null)
            {
                var logger = new NullLogger();
                var mg = new MagneticGraphics(logger);
                var mag = new MagneticClient(
                    logger, 
                    this, 
                    afterPlay, 
                    new MagneticIO(this),
                    mg
                    );
                mag.ms_init(
                    game.MagFile,
                    game.HntFile,
                    game.SndFile
                );
                mg.InitGfx(game.GfxFile, mag.version, mag.v4_id);

                if (gameInstances.ContainsKey(connectionId2))
                {
                    gameInstances[connectionId2] = mag;
                }
                else
                {
                    gameInstances.Add(connectionId2, mag);
                }
                var t = new Thread(new ThreadStart(mag.run));
                t.Start();
            }
        }

        public void OutputText(string text)
        {
            Clients.Caller.outputText(text);
        }

        private static Dictionary<string, MagneticClient> gameInstances = new Dictionary<string, MagneticClient>();

        internal void OutputStatus(string status)
        {
            Clients.Caller.outputStatus(status);
        }

        internal void ShowPicture(string gameId, int c, byte mode)
        {
            Clients.Caller.showPicture(gameId, c, mode);
        }
    }
}