﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;

namespace Magnetic.NET.WebUI
{
    public class MagneticIO : IMagneticIO
    {
        private MagneticGameHub hub;
        public MagneticIO(MagneticGameHub hub)
        {
            this.hub = hub;
        }

        private StringBuilder statusbuffer = new StringBuilder();
        public void ms_statuschar(char c)
        {
            this.statusbuffer.Append(c);
            if (c == '\n')
            {
                hub.OutputStatus(this.statusbuffer.ToString());
                this.statusbuffer.Clear();
            }
        }

        private StringBuilder outputbuffer = new StringBuilder();

        public void ms_putchar(char c)
        {
            this.outputbuffer.Append(c);
            if (c == '\n')
            {
                hub.OutputText(this.outputbuffer.ToString());
                this.outputbuffer.Clear();
            }
        }

        public void ms_flush()
        {
            if (this.statusbuffer.Length > 0)
            {
                hub.OutputStatus(this.statusbuffer.ToString());
                this.statusbuffer.Clear();
            }
            if (this.outputbuffer.Length > 0)
            {
                hub.OutputText(this.outputbuffer.ToString());
                this.outputbuffer.Clear();
            }
        }

        public void SetInput(string input)
        {
            lock (inputBufferLock)
            {
                inputBuffer = new Queue<char>();
                foreach (var charInput in input)
                {
                    inputBuffer.Enqueue(charInput);
                }
            }
        }

        private object inputBufferLock = new object();
        private Queue<char> inputBuffer = null;

        public char ms_getchar(int trans)
        {
            while (inputBuffer == null)
            {
                Thread.Sleep(100);
            }
            lock (inputBufferLock)
            {
                if (inputBuffer.Count == 0)
                {
                    inputBuffer = null;
                    return '\n';
                }
                else
                {
                    return inputBuffer.Dequeue();
                }
            }
        }


    }
}