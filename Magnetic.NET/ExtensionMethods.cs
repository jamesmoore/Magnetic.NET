﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnetic.NET
{
    static class ExtensionMethods
    {
        public static char[] toCharArray(this string thisString)
        {
            return thisString.ToCharArray();
        }
        public static int compareTo(this string thisString, string operand)
        {
            return thisString.CompareTo(operand);
        }
        public static bool equals(this string thisString, string operand)
        {
            return thisString == operand;
        }
        public static bool equalsIgnoreCase(this string thisString, string operand)
        {
            return thisString.Equals(operand, StringComparison.CurrentCultureIgnoreCase);
        }
        public static bool startsWith(this string thisString, string operand)
        {
            return thisString.StartsWith(operand);
        }
        public static string toUpperCase(this string thisString)
        {
            return thisString.ToUpper();
        }
        public static string trim(this string thisString)
        {
            return thisString.Trim();
        }
        public static int length(this string thisString)
        {
            return thisString.Length;
        }
        public static string substring(this string thisString, int a, int b)
        {
            return thisString.Substring(a, b);
        }


        public static StringBuilder append(this StringBuilder sb, string operand)
        {
            return sb.Append(operand);
        }
        public static string toString(this StringBuilder sb)
        {
            return sb.ToString();
        }

        public static uint Read32(this byte[] data, uint off)
        {

            uint tmp1 = (uint)data[off] << 24;
            uint tmp2 = (uint)data[off + 1] << 16;
            uint tmp3 = (uint)data[off + 2] << 8;
            uint tmp4 = (uint)data[off + 3];
            uint res = (uint)(tmp1 | tmp2 | tmp3 | tmp4);
            return res;
        }

        public static ushort Read16(this byte[] data, uint off)
        {
            ushort res;
            res = (ushort)(data[off] << 8 | data[off + 1]);
            return res;
        }

        public static byte Read8(this byte[] srcarr, uint off)
        {
            return (srcarr[off]);
        }
        public static void Write32(this byte[] data, uint off, uint val)
        {
           

            data[off + 3] = ((byte)val);
            val >>= /*TS*/ 8;
            data[off + 2] = ((byte)val);
            val >>= /*TS*/ 8;
            data[off + 1] = ((byte)val);
            val >>= /*TS*/ 8;
            data[off] = ((byte)val);
        }

        public static void Write16(this byte[] data, uint off, ushort val)
        {
            data[off + 1] = ((byte)val);
            val >>= /*TS*/ 8;
            data[off] = ((byte)val);
        }

        public static void Write8(this byte[] desarr, uint off, byte val)
        {
            desarr[off] = val;
        }



        static readonly int[] Empty = new int[0];

        public static IEnumerable<int> Locate(this byte[] self, byte[] candidate)
        {
            if (IsEmptyLocate(self, candidate) == false)
            {
                for (int i = 0; i < self.Length; i++)
                {
                    if (!IsMatch(self, i, candidate))
                        continue;

                    yield return i;
                }
            }

        }

        static bool IsMatch(byte[] array, int position, byte[] candidate)
        {
            if (candidate.Length > (array.Length - position))
                return false;

            for (int i = 0; i < candidate.Length; i++)
                if (array[position + i] != candidate[i])
                    return false;

            return true;
        }

        static bool IsEmptyLocate(byte[] array, byte[] candidate)
        {
            return array == null
                || candidate == null
                || array.Length == 0
                || candidate.Length == 0
                || candidate.Length > array.Length;
        }
    }
}
