﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnetic.NET
{
    public class GameInfo
    {
        public string ShortName { get; set; }
        public string MagFile { get; set; }
        public string GfxFile { get; set; }
        public string HntFile { get; set; }
        public string SndFile { get; set; }

        public static IEnumerable<GameInfo> ScanForGame()
        {
            return ScanForGame(".");
        }

        public static IEnumerable<GameInfo> ScanForGame(string path)
        {
            var magfiles = Directory.GetFiles(path, "*.mag", SearchOption.AllDirectories);

            foreach (var magfile in magfiles.Select(p => p.ToLower()))
            {
                var info = new GameInfo();

                var shortname = Path.GetFileNameWithoutExtension(magfile);
                info.ShortName = shortname;

                info.MagFile = magfile;
                info.GfxFile = magfile.Replace(".mag", ".gfx");
                if (File.Exists(info.GfxFile) == false) info.GfxFile = null;

                info.HntFile = magfile.Replace(".mag", ".hnt");
                if (File.Exists(info.HntFile) == false) info.HntFile = null;

                info.SndFile = magfile.Replace(".mag", ".snd");
                if (File.Exists(info.SndFile) == false) info.SndFile = null;

                yield return info;
            }
        }
    }
}
