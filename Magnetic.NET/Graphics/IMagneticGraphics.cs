﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnetic.NET.Graphics
{
    public interface IMagneticGraphics
    {
        MagneticPicture ms_extract(int pic, string picName);
        /* NEW */
        bool ms_animate(IList<JMagneticAniPos> positions);
        JMagneticAFProps ms_get_anim_frame(int number);
        bool ms_anim_is_repeating();
    }
}
