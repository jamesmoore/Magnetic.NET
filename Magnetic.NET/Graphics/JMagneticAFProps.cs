﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnetic.NET.Graphics
{
    public class JMagneticAFProps
    {
        public uint width { get; set; }
        public uint height  { get; set; }
        public byte[] FrameBytes { get; set; }
        public uint MaskByteWidth { get; set; }
        public byte[] MaskBytes { get; set; }
    }
}
