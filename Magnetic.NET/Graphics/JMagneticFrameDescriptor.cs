﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnetic.NET.Graphics
{
    public class JMagneticFrameDescriptor
    {
        public byte[] data;
        public uint offset;
        public uint data_size;
        public uint width;
        public uint height;
        public uint wbytes;
        public uint plane_step;
        public int mask;
        public bool isAnim;
        public uint pos_table_size;
    }
}
