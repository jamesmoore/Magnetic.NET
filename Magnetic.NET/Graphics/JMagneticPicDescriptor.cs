﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnetic.NET.Graphics
{
    public class JMagneticPicDescriptor
    {
        public JMagneticFrameDescriptor mainFrame;
        public bool isAnim;
        public ushort pos_table_size;
        public IList<JMagneticFrameDescriptor> animFrames;
        public IList<int> pos_table_count;
        public IList<IList<JMagneticAniPos>> pos_table; //ArrayList containing ArrayLists of AniPos
        public uint command_count;
        public uint command_table;
        public IList<Lookup> anim_table; // of type Lookup
        public short command_index;
        public bool anim_repeat;
        public short pos_table_index;
        public short pos_table_max;

        public JMagneticPicDescriptor()
        {
            this.mainFrame = new JMagneticFrameDescriptor();
            this.isAnim = false;
        }

        public void InitAnimContainer()
        {
            this.animFrames = new List<JMagneticFrameDescriptor>();
            this.pos_table_count = new List<int>();
            this.pos_table = new List<IList<JMagneticAniPos>>();
            this.anim_table = new List<Lookup>();
        }
    }

    public class Lookup
    {
        public short flag;
        public short count;

        public Lookup()
        {
            flag = -1;
            count = -1;
        }
    }
}
