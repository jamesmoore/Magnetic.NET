﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnetic.NET.Graphics
{
    public class MagneticGraphics : IMagneticGraphics
    {
        private bool LOGEMU = false;
        private bool LOGGFX = false;
        private bool LOGGFX_EXT = false;
        private bool LOGHNT = false;
        private bool LOGSND = false;

        byte gfx_ver = 0;
        uint gfx2_hsize = 0;
        byte[] gfx2_buf = null;
        byte[] gfx_data = null;
        byte[] gfx2_hdr = null;
        String gfx2_name;
        private JMagneticPicDescriptor main_pic;
        bool waitForAnims = false;
        int next_table = 1;
        RandomAccessFile gfx_fp = null;
        private ILogger gfxLogger;
        private int v4_id;
        public MagneticGraphics(ILogger log)
        {
            this.gfxLogger = log;
        }

        public byte InitGfx(String gfxname, byte version, int v4_id)
        {
            this.v4_id = v4_id;
            byte[] header2 = new byte[8];
            if (gfxname == null)
            {
                // Console.WriteLine("INIT done - no gfx.");
                if (LOGEMU)
                    log("logemu", "INIT done - no gfx.");
                return 1;
            }
            else
            {
                try
                {
                    gfx_fp = new RandomAccessFile(gfxname, "r");
                }
                catch (IOException ioe)
                {
                    ;
                }
                if (gfx_fp == null)
                    return 1;
            }

            if (gfx_fp.ReadUbyteFromFile(header2) != 8)
            {
                gfx_fp.close();
                gfx_fp = null;
                return 1;
            }

            if (version < 4 && read_l(header2, 0) == 0x4D615069) /* MaPi */
                return init_gfx1(header2);
            else if (version == 4 && read_l(header2, 0) == 0x4D615032) /* MaP2 */
                return init_gfx2(header2);
            gfx_fp.close();
            gfx_fp = null;
            // Console.WriteLine("INIT done - with gfx.");
            if (LOGEMU)
                log("logemu", "INIT done - with gfx.");
            return 1;
        }

        public MagneticPicture ms_extract(int pic, string picName)
        {
            switch (gfx_ver)
            {
                case 1:
                    return ms_extract1(pic);
                case 2:
                    return ms_extract2(picName); // call code[pic], because here pic
                // is offset
            }
            return null;
        }

        bool is_blank(ushort line, ushort width, byte[] gfx_buf)
        {
            for (var i = line * width; i < ((line + 1) * width); i++)
                if (gfx_buf[i] != 0)
                    return false;
            return true;
        }

        MagneticPicture ms_extract1(int pic)
        {
            byte bit;
            byte val;
            uint tablesize, count;
            uint i, j;
            uint datasize, upsize, offset, buffer, decode_table, data;
            ushort w, h;
            ushort[] pal = new ushort[16];

            // if (gfx_buf == null) return 0;

            offset = read_l(gfx_data, 4 * (uint)pic);
            buffer = offset - 8;

            for (i = 0; i < 16; i++)
                pal[i] = read_w(gfx_data, buffer + 0x1c + 2 * i);
            w = (ushort)(read_w(gfx_data, buffer + 4) - read_w(gfx_data, buffer + 2));
            h = read_w(gfx_data, buffer + 6);
            var gfx_buf = new byte[w * h];
            tablesize = read_w(gfx_data, buffer + 0x3c);
            datasize = read_l(gfx_data, buffer + 0x3e);
            decode_table = buffer + 0x42;
            data = decode_table + tablesize * 2 + 2;
            upsize = (uint)(h * w);

            for (i = 0, j = 0, count = 0, val = 0, bit = 7; i < upsize; i++, count--)
            {
                if (count == 0)
                {
                    count = tablesize;
                    while (count < 0x80)
                    {
                        if ((gfx_data[data + j] & (1 << bit)) != 0)
                            count = gfx_data[decode_table + 2 * count];
                        else
                            count = gfx_data[decode_table + 2 * count + 1];
                        if (bit == 0)
                            j++;
                        bit = (byte)((bit != 0) ? bit - 1 : 7);
                    }
                    count &= 0x7f;
                    if (count >= 0x10)
                        count -= 0x10;
                    else
                    {
                        val = (byte)count;
                        count = 1;
                    }
                }
                gfx_buf[i] = val;
            }
            for (j = w; j < upsize; j++)
                gfx_buf[j] ^= gfx_buf[j - w];

            for (; h > 0 && (is_blank((ushort)(h - 1), w, gfx_buf)); h--)
                ;
            for (i = 0; h > 0 && (is_blank((ushort)i, w, gfx_buf)); h--, i++)
                ;
            //if (i != 0)
            //	Console.WriteLine("Bild gefunden");
            return new MagneticPicture()
            {
                CurrPalette = pal,
                CurrPHeight = h,
                CurrPWidth = w,
                gfx_buf = gfx_buf,
            };
        }

        short find_name_in_header(String name, bool upper)
        {
            short header_pos = 0;

            if (upper)
            {
                name = name.toUpperCase();
            }

            if (name.length() > 6)
                name = name.substring(0, 6);

            while (header_pos < gfx2_hsize)
            {
                var namearray = new byte[name.Length];
                Array.Copy(gfx2_hdr, header_pos, namearray, 0, name.Length);
                String hname = Encoding.ASCII.GetString(namearray);
                if (hname.compareTo(name) == 0)
                    return header_pos;
                header_pos += 16;
            }
            return -1;
        }

        byte[] extract_frame(JMagneticFrameDescriptor frame)
        {
            int i, mask, value;
            int[] values = new int[4];

            if (frame.width * frame.height > 51200)
            {
                throw new ApplicationException("picture too large");
                return null;
            }
            else
            {
                var gfx_buf_frame = new byte[frame.width * frame.height];
                for (var y = 0; y < frame.height; y++)
                {
                    var ywb = y * (int)frame.wbytes;
                    var yw = y * (int)frame.width;

                    for (int x = 0; x < frame.width; x++)
                    {
                        if ((x % 8) == 0)
                        {
                            for (i = 0; i < 4; i++)
                                values[i] = frame.data[frame.offset + ywb + (x / 8)
                                        + (frame.plane_step * i)];
                        }

                        var bit_x = 7 - (x & 7);
                        mask = 1 << bit_x;
                        value = ((values[0] & mask) >> bit_x) << 0
                                | ((values[1] & mask) >> bit_x) << 1
                                | ((values[2] & mask) >> bit_x) << 2
                                | ((values[3] & mask) >> bit_x) << 3;
                        value &= 15;

                        gfx_buf_frame[yw + x] = (byte)value;
                    }
                }
                return gfx_buf_frame;
            }
        }
        //
        // Animation stuff
        //
        MagneticPicture ms_extract2(string name)
        {
            main_pic = new JMagneticPicDescriptor();
            uint offset = 0, length = 0, i;
            short header_pos = -1;
            ushort[] pal = new ushort[16];

            uint anim_data;
            long j;

            // Console.WriteLine(codeOffset);
            // log("logemu","INIT done - no gfx.");
            if (string.IsNullOrEmpty(name) == false)
            {

                gfx2_name = name;

                // Console.WriteLine(name);

                main_pic.isAnim = false;

                main_pic.pos_table_size = 0;
                if (gfx2_name.equals("outkit"))
                {
                    int k = 98;
                }

                /* Find the uppercase (no animation) version of the picture first. */
                header_pos = find_name_in_header(name, true);

                // if (header_pos < 0)
                var ani_header_pos = find_name_in_header(name, false);
                if (ani_header_pos >= 0)
                    header_pos = ani_header_pos;
                if (header_pos < 0)
                {
                    return null;
                }

                // Console.WriteLine( "headerpos: "+header_pos+"\n");
                offset = read_l(gfx2_hdr, (uint)header_pos + 8);
                length = read_l(gfx2_hdr, (uint)header_pos + 12);

                if (offset != 0)
                {
                    if (gfx2_buf != null)
                    {
                        gfx2_buf = null;
                    }

                    gfx2_buf = new byte[length];

                    gfx_fp.seek(offset);

                    if (gfx_fp.ReadUbyteFromFile(gfx2_buf, length) != length)
                    {
                        gfx2_buf = null;
                        throw new ApplicationException("Graphics file length read mismatch");
                    }

                    for (i = 0; i < 16; i++)
                        pal[i] = read_w2(gfx2_buf, 4 + (2 * i));

                    main_pic.mainFrame.data = gfx2_buf;
                    main_pic.mainFrame.offset = 48;
                    main_pic.mainFrame.data_size = read_l2(gfx2_buf, 38);
                    main_pic.mainFrame.width = read_w2(gfx2_buf, 42);
                    main_pic.mainFrame.height = read_w2(gfx2_buf, 44);
                    main_pic.mainFrame.wbytes = (main_pic.mainFrame.data_size / main_pic.mainFrame.height);
                    main_pic.mainFrame.plane_step = (main_pic.mainFrame.wbytes / 4);
                    main_pic.mainFrame.mask = -1;
                    var frameBytes = extract_frame(main_pic.mainFrame);

                    var returnvalue = new MagneticPicture();
                    returnvalue.gfx_buf = frameBytes;
                    returnvalue.CurrPHeight = main_pic.mainFrame.height;
                    returnvalue.CurrPWidth = main_pic.mainFrame.width;
                    returnvalue.CurrPalette = pal;

                    anim_data = 48 + main_pic.mainFrame.data_size; // relative to
                    // gfx2_buf
                    if ((gfx2_buf[anim_data] != 0xD0)
                            || (gfx2_buf[anim_data + 1] != 0x5E))
                    {
                        uint current;
                        uint frame_count;
                        uint value1, value2;

                        main_pic.isAnim = true;
                        main_pic.InitAnimContainer();

                        current = anim_data + 6; // Offset to gfx2_buf
                        frame_count = read_w2(gfx2_buf, anim_data + 2);

                        for (i = 0; i < frame_count; i++)
                        {
                            JMagneticFrameDescriptor newFrame = new JMagneticFrameDescriptor();
                            // Must be tested!!!
                            newFrame.data = gfx2_buf;
                            newFrame.offset = current + 10;
                            newFrame.data_size = read_l2(gfx2_buf, current);
                            newFrame.width = read_w2(gfx2_buf, current + 4);
                            newFrame.height = read_w2(gfx2_buf, current + 6);
                            newFrame.wbytes = (newFrame.data_size / newFrame.height);
                            newFrame.plane_step = (newFrame.wbytes / 4);
                            newFrame.mask = -1;

                            current += newFrame.data_size + 12;
                            value1 = read_w2(gfx2_buf, current - 2);
                            value2 = read_w2(gfx2_buf, current);

                            if ((value1 == newFrame.width)
                                    && (value2 == newFrame.height))
                            {
                                uint skip;

                                newFrame.mask = (int)(current + 4);
                                skip = read_w2(gfx2_buf, current + 2);
                                current += (skip + 6);
                            }

                            main_pic.animFrames.Add(newFrame);
                        }

                        main_pic.pos_table_size = read_w2(gfx2_buf, current - 2);
                        for (int x = 0; x < main_pic.pos_table_size; x++)
                            main_pic.anim_table.Add(new Lookup());

                        if (LOGGFX_EXT)
                        {
                            log("loggfx", "POSITION TABLE DUMP\n");
                        }

                        for (i = 0; i < main_pic.pos_table_size; i++)
                        {
                            uint newPos = read_w2(gfx2_buf, current + 2);
                            main_pic.pos_table_count.Add((int)newPos);
                            current += 4;

                            // new Position table for current frame
                            var currTable = new List<JMagneticAniPos>();

                            int elCount = main_pic.pos_table_count[(int)i];

                            for (j = 0; j < elCount; j++)
                            {
                                JMagneticAniPos newAni = new JMagneticAniPos();
                                newAni.x = (short)read_w2(gfx2_buf, current);
                                newAni.y = (short)read_w2(gfx2_buf, current + 2);
                                newAni.number = (short)(read_w2(gfx2_buf, current + 4) - 1);
                                current += 8;
                                if (LOGGFX_EXT)
                                {
                                    // Console.WriteLine("Position entry: Table:
                                    // "+i+" Entry: "+j+" X: "+newAni.x+" Y:
                                    // "+newAni.y+" Frame: "+newAni.number+"\n");
                                    log("loggfx", "Position entry: Table: " + i
                                            + "  Entry: " + j + "  X: " + newAni.x
                                            + " Y: " + newAni.y + " Frame: "
                                            + newAni.number + "\n");
                                }
                                currTable.Add(newAni);
                            }

                            main_pic.pos_table.Add(currTable);
                        }

                        main_pic.command_count = read_w2(gfx2_buf, current);
                        main_pic.command_table = current + 2;

                        main_pic.command_index = 0;
                        main_pic.anim_repeat = false;
                        main_pic.pos_table_index = -1;
                        main_pic.pos_table_max = -1;
                        // main_pic.anim_table.clear();
                    }
                    //return 99;
                    return returnvalue;
                }
                //return 0;
            }
            return null; // 0
        }

        public bool ms_animate(IList<JMagneticAniPos> positions /* of type JMagenticAniPos */)
        {
            bool got_anim = false;
            int i, ttable;
            int count = 0;

            if ((main_pic == null) || (gfx_ver != 2))
                return false;
            if ((main_pic.pos_table_size == 0) || (main_pic.command_index < 0))
                return false;

            positions.Clear();

            while (!got_anim)
            {
                if (main_pic.pos_table_max >= 0)
                {
                    if (main_pic.pos_table_index < main_pic.pos_table_max)
                    {
                        for (i = 0; i < main_pic.pos_table_size; i++)
                        {
                            var lu = main_pic.anim_table[i];
                            if (lu.flag > -1)
                            {
                                var currPosArray = main_pic.pos_table[i];
                                var newPos = currPosArray[lu.flag];
                                // ### Ist das richtig? pos_array[count] =
                                // pos_table[i][anim_table[i].flag];
                                if (LOGGFX_EXT)
                                {
                                    log("loggfx", "Adding frame " + newPos.number
                                            + " to buffer");
                                }
                                count++;

                                int currPostTableCount = main_pic.pos_table_count[i];

                                if (lu.flag < (currPostTableCount - 1))
                                    lu.flag++;
                                if (lu.count > 0)
                                    lu.count--;
                                else
                                    lu.flag = -1;

                                // write back
                                main_pic.anim_table[i] = lu;

                                positions.Add(newPos);
                            }
                        }
                        if (count > 0)
                        {
                            // *positions = pos_array;
                            got_anim = true;
                        }
                        main_pic.pos_table_index++;
                    }
                }

                if (!got_anim)
                {
                    byte command = gfx2_buf[main_pic.command_table
                            + main_pic.command_index];
                    main_pic.command_index++;

                    main_pic.pos_table_max = -1;
                    main_pic.pos_table_index = -1;
                    // main_pic.anim_table.clear();

                    switch (command)
                    {
                        case 0x00:
                            main_pic.command_index = -1;
                            return false;
                        case 0x01:
                            if (LOGGFX_EXT)
                            {
                                log("loggfx", "Load Frame Table: "
                                        + (int)command
                                        + " Start at: "
                                        + (int)gfx2_buf[main_pic.command_table
                                                + main_pic.command_index + 1]
                                        + " Count: "
                                        + (int)gfx2_buf[main_pic.command_table
                                                + main_pic.command_index + 2]);
                            }

                            ttable = gfx2_buf[main_pic.command_table
                                    + main_pic.command_index];
                            main_pic.command_index++;

                            var lu = main_pic.anim_table[ttable - 1];

                            // int offset1 =
                            // main_pic.command_table+main_pic.command_index;
                            // byte fbyte = gfx2_buf[main_pic.command_table];
                            // byte fbyte2 = gfx2_buf[offset1];
                            lu.flag = (short)((gfx2_buf[main_pic.command_table
                                    + main_pic.command_index]) - 1);
                            main_pic.command_index++;
                            // int offset2 =
                            // main_pic.command_table+main_pic.command_index;
                            // byte fbyte3 = gfx2_buf[offset2];

                            lu.count = (short)((gfx2_buf[main_pic.command_table
                                    + main_pic.command_index]) - 1);
                            main_pic.command_index++;

                            /* Workaround for Wonderland "catter" animation */
                            if (v4_id == 0)
                            {
                                if (gfx2_name.equalsIgnoreCase("catter"))
                                {
                                    if (main_pic.command_index == 96)
                                        lu.count = 9;
                                    if (main_pic.command_index == 108)
                                        lu.flag = -1;
                                    if (main_pic.command_index == 156)
                                        lu.flag = -1;
                                }
                            }

                            // write back
                            main_pic.anim_table[ttable - 1] = lu;

                            break;
                        case 0x02:
                            if (LOGGFX_EXT)
                            {
                                log("loggfx", "Animate: "
                                        + (int)gfx2_buf[main_pic.command_table
                                                + main_pic.command_index]);
                            }
                            main_pic.pos_table_max = gfx2_buf[main_pic.command_table
                                    + main_pic.command_index];
                            main_pic.pos_table_index = 0;
                            main_pic.command_index++;
                            break;
                        case 0x03:
                            if (LOGGFX_EXT)
                            {
                                log("loggfx", "Stop/Repeat Param: "
                                        + (int)gfx2_buf[main_pic.command_table
                                                + main_pic.command_index]);
                                main_pic.command_index = -1;
                                return false;
                            }
                            else
                            {
                                if (v4_id == 0)
                                {
                                    main_pic.command_index = -1;
                                    return false;
                                }
                                else
                                {
                                    if (!this.waitForAnims)
                                    {
                                        main_pic.command_index = 0;
                                        main_pic.anim_repeat = true;
                                        main_pic.pos_table_index = -1;
                                        main_pic.pos_table_max = -1;
                                    }
                                    else
                                    {
                                        main_pic.command_index = -1;
                                        return false;
                                    }

                                    // main_pic.anim_table.clear();
                                    /*
                                     * for (j = 0; j < MAX_POSITIONS; j++) {
                                     * anim_table[j].flag = -1; anim_table[j].count =
                                     * -1; }
                                     */
                                }
                                break;
                            }
                        case 0x04:
                            if (LOGGFX_EXT)
                            {
                                log("loggfx", "Unknown Command: "
                                        + command
                                        + " Prop1: "
                                        + gfx2_buf[main_pic.command_table
                                                + main_pic.command_index]
                                        + "  Prop2: "
                                        + gfx2_buf[main_pic.command_table
                                                + main_pic.command_index + 1]
                                        + " WARNING:not parsed");
                            }
                            main_pic.command_index += 3;
                            return false;
                        case 0x05:
                            ttable = next_table;
                            main_pic.command_index++;

                            var lu05 = (Lookup)main_pic.anim_table[ttable - 1];

                            lu05.flag = 0;
                            lu05.count = gfx2_buf[main_pic.command_table
                                    + main_pic.command_index];

                            // write back
                            main_pic.anim_table[ttable - 1] = lu05;

                            main_pic.pos_table_max = gfx2_buf[main_pic.command_table
                                    + main_pic.command_index];
                            main_pic.pos_table_index = 0;
                            main_pic.command_index++;
                            main_pic.command_index++;
                            next_table++;
                            break;
                        default:
                            throw new ApplicationException("unknown animation command");
                            main_pic.command_index = -1;
                            return false;
                    }
                }
            }
            if (LOGGFX_EXT)
            {
                log("loggfx", "ms_animate() returning " + count + " frames");
            }
            return got_anim;
        }

        public JMagneticAFProps ms_get_anim_frame(int number)
        {
            JMagneticAFProps props = new JMagneticAFProps();
            if (number >= 0)
            {
                try
                {
                    var frame = main_pic.animFrames[number];
                    var frameBytes = extract_frame(frame);
                    props.FrameBytes = frameBytes;
                    props.width = frame.width;
                    props.height = frame.height;

                    if (frame.mask > 0)
                    {
                        uint iByteWidth = 0;
                        //// calc mask size
                        var iModulo = frame.width % 16;
                        var iWidth = frame.width + ((iModulo > 0) ? (16 - iModulo) : 0);
                        iByteWidth = iWidth / 8;
                        props.MaskByteWidth = iByteWidth;
                        props.MaskBytes = new byte[iByteWidth * frame.height];
                        Array.Copy(this.gfx2_buf, frame.mask, props.MaskBytes, 0, iByteWidth * frame.height);
                    }

                }
                catch (Exception e)
                {
                }
                return props;
            }
            return null;
        }

        public bool ms_anim_is_repeating()
        {
            return main_pic.anim_repeat;
        }

        byte init_gfx1(byte[] header2)
        {
            gfx_data = new byte[read_l(header2, 4) - 8];
            if (gfx_data == null)
            {
                gfx_fp.close();
                gfx_fp = null;
                return 1;
            }
            if (gfx_fp.ReadUbyteFromFile(gfx_data) != gfx_data.Length)
            {
                gfx_fp.close();
                gfx_data = null;
                gfx_fp = null;
                return 1;
            }

            gfx_fp.close();
            gfx_fp = null;

            gfx_ver = 1;
            return 2;
        }

        byte init_gfx2(byte[] header)
        {
            gfx2_hsize = read_w(header, 4);
            gfx2_hdr = new byte[gfx2_hsize];
            if (gfx2_hdr == null)
            {
                gfx_fp.close();
                gfx_fp = null;
                return 1;
            }

            gfx_fp.seek(6);

            if (gfx_fp.ReadUbyteFromFile(gfx2_hdr, gfx2_hsize) != gfx2_hsize)
            {
                gfx_fp.close();
                gfx2_hdr = null;
                gfx_fp = null;
                return 1;
            }

            gfx_ver = 2;
            return 2;
        }

        uint read_l(byte[] data, uint off)
        {

            uint tmp1 = (uint)data[off] << 24;
            uint tmp2 = (uint)data[off + 1] << 16;
            uint tmp3 = (uint)data[off + 2] << 8;
            uint tmp4 = (uint)data[off + 3];
            uint res = tmp1 | tmp2 | tmp3 | tmp4;
            return res;
        }

        uint read_l2(byte[] data, uint off)
        {

            uint tmp1 = (uint)data[off + 1] << 24;
            uint tmp2 = (uint)data[off] << 16;
            uint tmp3 = (uint)data[off + 3] << 8;
            uint tmp4 = (uint)data[off + 2];
            uint res = tmp1 | tmp2 | tmp3 | tmp4;
            return res;
        }

        ushort read_w(byte[] data, uint off)
        {
            ushort res;
            res = (ushort)(data[off] << 8 | data[off + 1]);
            return res;
        }
        ushort read_w2(byte[] data, int off)
        {
            if (off < 0) throw new ArgumentException("Offset < 0");
            return read_w2(data, (uint)off);
        }
        ushort read_w2(byte[] data, uint off)
        {
            ushort res;
            res = (ushort)(data[off + 1] << 8 | data[off]);
            return res;
        }

        void log(String cat, String s)
        {
            if (cat.startsWith("loggfx"))
            {
                if (gfxLogger == null)
                    gfxLogger = Logger.getLogger("loggfx");
                gfxLogger.info(s);
            }
            else
            {
                Logger.getRootLogger().warn(s);
            }
            // Console.WriteLine(s);
        }
    }
}
