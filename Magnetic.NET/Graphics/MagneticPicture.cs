﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnetic.NET.Graphics
{
    public class MagneticPicture
    {
        public byte[] gfx_buf;
        public uint CurrPHeight;
        public uint CurrPWidth;
        public ushort[] CurrPalette;
    }
}
