﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnetic.NET.Hint
{
    public class JMagneticHint
    {
        public uint elcount;
        public uint nodetype;
        public IList<string> content;
        public IList<uint> links;
        public uint parent;

        public JMagneticHint()
        {
            this.content = new List<string>();
            this.links = new List<uint>();
        }
    }
}
