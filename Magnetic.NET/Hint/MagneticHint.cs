﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnetic.NET.Hint
{
    public class MagneticHint
    {
        private IMagneticIO io;
        public MagneticHint(IMagneticIO io)
        {
            this.io = io;
        }

        private bool LOGEMU = false;
        private bool LOGGFX = false;
        private bool LOGGFX_EXT = false;
        private bool LOGHNT = false;
        private bool LOGSND = false;

        private ILogger hntLogger;

        /* Hint support */
        IList<JMagneticHint> hints = new List<JMagneticHint>(); // Type Hint

        public bool HasHints()
        {
            return hints != null && hints.Any();
        }

        public void init_hint(String hntname)
        {
            var hnt_fp = new RandomAccessFile(hntname, "r");
            if (hnt_fp == null)
            {
                // Console.WriteLine("Unable to open hint file
                // "+hntname);
                if (LOGEMU)
                    log("logemu", "Unable to open hint file " + hntname);
            }
            else
            {
                // if ((ReadUbyteFromFile(fp,header) != 42) ||
                // (read_l(header,0) != 0x4d615363)) {
                byte[] header3 = new byte[4];
                if ((hnt_fp.ReadUbyteFromFile( header3) == 4)
                        && (read_l(header3, 0) == 0x4D614874))
                {
                    byte[] buf = new byte[8];
                    uint blkcnt, elcnt, ntype, elsize;

                    /* Read number of blocks */
                    hnt_fp.ReadUbyteFromFile( buf, 2);
                    blkcnt = read_w2(buf, 0);
                    if (LOGHNT)
                    {
                        log("loghnt", "Blocks: " + blkcnt);
                    }
                    for (var i = 0; i < blkcnt; i++)
                    {
                        JMagneticHint newHint = new JMagneticHint();
                        if (LOGHNT)
                        {
                            log("loghnt", "Block No. " + i);
                        }
                        /* Read number of elements */
                        hnt_fp.ReadUbyteFromFile( buf, 2);
                        elcnt = read_w2(buf, 0);
                        if (LOGHNT)
                        {
                            log("loghnt", "Elements: " + elcnt);
                        }
                        newHint.elcount = elcnt;

                        /* Read node type */
                        hnt_fp.ReadUbyteFromFile( buf, 2);
                        ntype = read_w2(buf, 0);
                        if (LOGHNT)
                        {
                            if (ntype == 1)
                                log("loghnt", "Type: Node");
                            else
                                log("loghnt", "Type: Leaf");
                        }
                        newHint.nodetype = ntype;
                        if (LOGHNT)
                        {
                            log("loghnt", "Elements:\n");
                        }
                        for (int j = 0; j < elcnt; j++)
                        {
                            hnt_fp.ReadUbyteFromFile( buf, 2);
                            elsize = read_w2(buf, 0);
                            byte[] newEl = new byte[elsize];
                            hnt_fp.ReadUbyteFromFile( newEl, elsize);
                            newHint.content.Add(Encoding.ASCII.GetString(newEl));
                            if (LOGHNT)
                            {
                                log("loghnt", Encoding.ASCII.GetString(newEl));
                            }
                        }
                        /* Do we need a jump table? */
                        if (ntype == 1)
                        {
                            if (LOGHNT)
                            {
                                log("loghnt", "Jump to block:\n");
                            }
                            for (int j = 0; j < elcnt; j++)
                            {
                                hnt_fp.ReadUbyteFromFile( buf, 2);
                                uint link = read_w2(buf, 0);
                                newHint.links.Add(link);
                                if (LOGHNT)
                                {
                                    log("loghnt", "" + link);
                                }
                            }
                        }

                        /* Read the parent block */
                        hnt_fp.ReadUbyteFromFile( buf, 2);
                        newHint.parent = read_w2(buf, 0);
                        if (LOGHNT)
                        {
                            log("loghnt", "Parent: " + newHint.parent);
                        }
                        this.hints.Add(newHint);
                    }
                }
                hnt_fp.close();
            }
        }

        public ushort show_hints_text()
        {
            return this.show_hints_text(hints, 0);
        }

        private ushort show_hints_text(IList<JMagneticHint> hints, short index)
        {
            int i = 0, j = 0;
            short input;
            var hint = hints[index];

            while (true)
            {
                switch (hint.nodetype)
                {

                    case 1: /* folders */
                        output_text("Hint categories:\n".toCharArray());
                        for (i = 0; i < hint.elcount; i++)
                        {
                            output_number(i + 1);
                            output_text(". ".toCharArray());
                            output_text((hint.content[i]).toCharArray());
                            // io.ms_putchar('\n');
                        }
                        output_text("Enter hint category number, ".toCharArray());
                        if (hint.parent >= 0) // Differs from emu.c
                            output_text("P for the parent hint menu, ".toCharArray());
                        output_text("or E to end hints.\n".toCharArray());

                        input = hint_input();
                        switch (input)
                        {
                            case -1: /* A new game is being loaded */
                                return 1;
                            case -2: /* End hints */
                                return 1;
                            case -4: /* Show parent hint list */
                                if (hint.parent >= 0) // Differs from emu.c
                                    return 0;
                                if ((input > 0) && (input <= hint.elcount))
                                {
                                    var element = hint.links[input - 1];
                                    if (show_hints_text(hints, (short)element) == 1)
                                        return 1;
                                }
                                break;
                            default:
                                if ((input > 0) && (input <= hint.elcount))
                                {
                                    var element = hint.links[input - 1];
                                    if (show_hints_text(hints, (short)element) == 1)
                                        return 1;
                                }
                                break;
                        }
                        break;

                    case 2: /* hints */
                        if (i < hint.elcount)
                        {
                            output_number(i + 1);
                            output_text(". ".toCharArray());
                            output_text(hint.content[i].toCharArray());

                            if (i == hint.elcount - 1)
                            {
                                output_text("\nNo more hints.\n".toCharArray());
                                return 0; /* Last hint */
                            }
                            else
                            {
                                output_text("\nEnter N for the next hint, "
                                        .toCharArray());
                                output_text("P for the parent hint menu, "
                                        .toCharArray());
                                output_text("or E to end hints.\n".toCharArray());
                            }

                            input = hint_input();
                            switch (input)
                            {
                                case -1: /* A new game is being loaded */
                                    return 1;
                                case -2: /* End hints */
                                    return 1;
                                case -3:
                                    i++;
                                    break;
                                case -4: /* Show parent hint list */
                                    return 0;
                            }
                        }
                        else
                            return 0;
                        break;
                }
            }
            // return 0;
        }

        void log(String cat, String s)
        {
            if (cat.startsWith("loghnt"))
            {
                if (hntLogger == null)
                    hntLogger = Logger.getLogger("loghnt");
                hntLogger.info(s);
            }
            else
            {
                Logger.getRootLogger().warn(s);
            }
            // Console.WriteLine(s);
        }

        ushort read_w2(byte[] data, int off)
        {
            if (off < 0) throw new ArgumentException("Offset < 0");
            return read_w2(data, (uint)off);
        }

        void output_number(int number)
        {
            int tens = number / 10;

            if (tens > 0)
                io.ms_putchar((char)('0' + tens));
            number -= (tens * 10);
            io.ms_putchar((char)('0' + number));
        }

        ushort read_w2(byte[] data, uint off)
        {
            ushort res;
            res = (ushort)(data[off + 1] << 8 | data[off]);
            return res;
        }

        short hint_input()
        {
            char c1 = '\0', c2 = '\0', c3 = '\0';

            output_text(">>".toCharArray());
            io.ms_flush();

            do
            {
                c1 = io.ms_getchar(0);
            } while (c1 == '\n');
            if (c1 == 1)
                return -1; /* New game loaded, abort hints */

            c2 = io.ms_getchar(0);
            if (c2 == 1)
                return -1;

            /* Get and discard any remaining byteacters */
            c3 = c2;
            while (c3 != '\n')
            {
                c3 = io.ms_getchar(0);
                if (c3 == 1)
                    return -1;
            }
            io.ms_putchar('\n');

            if ((c1 >= '0') && (c1 <= '9'))
            {
                var number = c1 - '0';
                if ((c2 >= '0') && (c2 <= '9'))
                {
                    number *= 10;
                    number += c2 - '0';
                }
                return (short)number;
            }

            if ((c1 >= 'A') && (c1 <= 'Z'))
                c1 = (char)('a' + (c1 - 'A'));
            if ((c1 >= 'a') && (c1 <= 'z'))
            {
                switch (c1)
                {
                    case 'e':
                        return -2; /* End hints */
                    case 'n':
                        return -3; /* Next hint */
                    case 'p':
                        return -4; /* Show parent hint list */
                }
            }
            return 0;
        }

        uint read_l(byte[] data, uint off)
        {

            uint tmp1 = (uint)data[off] << 24;
            uint tmp2 = (uint)data[off + 1] << 16;
            uint tmp3 = (uint)data[off + 2] << 8;
            uint tmp4 = (uint)data[off + 3];
            uint res = (uint)(tmp1 | tmp2 | tmp3 | tmp4);
            return res;
        }

        int output_text(char[] text)
        {
            int i;

            for (i = 0; i < text.Length; i++)
                io.ms_putchar(text[i]);
            return i;
        }
    }
}
