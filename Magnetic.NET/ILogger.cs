﻿using System;
namespace Magnetic.NET
{
    public interface ILogger
    {
        void debug(string p);
        void info(string s);
        void warn(string s);
    }
}
