﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnetic.NET
{
    public interface IMagneticIO
    {
        void ms_statuschar(char c);
        void ms_putchar(char c);
        void ms_flush();
        char ms_getchar(int trans);
    }
}
