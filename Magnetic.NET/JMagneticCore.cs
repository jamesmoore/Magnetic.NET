﻿using Magnetic.NET.Hint;
using Magnetic.NET.Sound;
using Magnetic.NET.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using StringBuffer = System.Text.StringBuilder;

namespace Magnetic.NET
{

    /****************************************************************************\
    *
    * Magnetic.NET - Magnetic Scrolls Interpreter ported to C# by James Moore
    *
    * Credits to Niclas Karlsson <nkarlsso@abo.fi>,
    *            David Kinder <davidk@davidkinder.co.uk>,
    *            Stefan Meier <Stefan.Meier@if-legends.org> and
    *            Paul David Doherty <pdd@if-legends.org>
    *
    *     This program is free software; you can redistribute it and/or modify
    *     it under the terms of the GNU General Public License as published by
    *     the Free Software Foundation; either version 2 of the License, or
    *     (at your option) any later version.
    *
    *     This program is distributed in the hope that it will be useful,
    *     but WITHOUT ANY WARRANTY; without even the implied warranty of
    *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    *     GNU General Public License for more details.
    *
    *     You should have received a copy of the GNU General Public License
    *     along with this program; if not, write to the Free Software
    *     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
    /****************************************************************************/
    public abstract class JMagneticCore : JMagneticUI
    {

        private bool LOGEMU = false;
        private bool LOGGFX = false;
        private bool LOGGFX_EXT = false;
        private bool LOGHNT = false;
        private bool LOGSND = false;

        private Logger emuLogger;
        private Logger gfxLogger;
        private Logger hntLogger;
        private Logger sndLogger;

        private Register32[] dreg = new Register32[8]; /* 8 32bit Register */
        private Register32[] areg = new Register32[8];
        private Register32[] allreg = new Register32[16];

        uint i_count;
        uint pc;
        uint rseed = 0;
        uint mem_size, string_size;
        ushort properties, fl_sub, fl_tab, fl_size, fp_tab, fp_size;
        bool zflag, nflag, cflag, vflag;
        byte byte1, byte2, regnr, opsize;
        AddressMode admode;
        uint arg1, arg2;
        byte[] acc1;
        byte[] acc2;
        byte lastbyte = 0;
        bool sdd;
        bool running;

        public byte version { get; private set; }

        byte[] restart = null, tstring = null, tstring2 = null,
                dict = null;
        byte[] code = null;
        uint decode_offset = 0;
        byte[] decode_src;
        bool quick_flag = false;

        static char[] undo_ok = "\n[Previous turn undone.]".toCharArray();
        static char[] undo_fail = "\n[You can't \"undo\" what hasn't been done!]"
                .toCharArray();
        bool big, period, pipe;
        uint offset_bak;
        byte mask_bak;
        uint[][] undo_regs = new uint[2][];
        uint undo_pc, undo_size;
        byte[][] undo = new byte[2][];
        byte[] undo_stat = new byte[2];

        public int v4_id { get; private set; }
        bool sndenabled = true;

        string[] gfxlist = {
            "bank"   /*Wonderland*/,
            "cstart" /*Corruption*/,
            "ffirst" /*Fish*/,
            "griver" /*Guild*/
        };

        /*
         * Abstract methods - push to interface abstract byte ms_load_file( byte[]
         * name, int offset, int size) throws IOException; abstract byte
         * ms_save_file( byte[] name, int offset, int size) throws IOException;
         * abstract void ms_statusbyte(byte c); abstract void ms_putchar(byte c);
         * abstract void ms_flush(); abstract byte ms_getchar(int trans) throws
         * IOException; abstract void ms_showpic(int c,byte mode); abstract void
         * ms_waitForKey(); abstract void ms_fatal(String txt); abstract int
         * ms_showhints(Vector hints);
         */

        /*
         * Vector anim_frame_table = new Vector(); // Type Picture int
         * pos_table_size = 0; Vector pos_table_count = new Vector(); Vector
         * pos_table = new Vector(); // Type Vector of AniPos int command_table = 0;
         * int command_index = -1; Vector anim_table = new Vector(); //Type Lookup
         * int pos_table_index = -1; int pos_table_max = -1; Vector pos_array = new
         * Vector(); // Type AniPos byte anim_repeat = 0;
         */



        static char[] no_hints = "[Hints are not available.]\n".toCharArray();
        static char[] not_supported = "[This function is not supported.]\n".toCharArray();

        private ILogger registerLogger;
        private IMagneticIO magneticio;
        private MagneticHint hints;
        private MagneticSound sound;
        public JMagneticCore(
            ILogger registerLogger,
            IMagneticIO magneticio
            )
        {
            this.registerLogger = registerLogger;
            this.magneticio = magneticio;
            for (int i = 0; i < 2; i++)
            {
                undo_regs[i] = new uint[18];
            }
        }

        // LOGGFX LOGHNT
        void log(String cat, String s)
        {
            if (cat.equals("logemu"))
            {
                if (emuLogger == null)
                    emuLogger = Logger.getLogger("logemu");
                emuLogger.info(s);
            }
            else if (cat.equals("loghnt"))
            {
                if (hntLogger == null)
                    hntLogger = Logger.getLogger("loghnt");
                hntLogger.info(s);
            }
            else if (cat.equals("logsnd"))
            {
                if (sndLogger == null)
                    sndLogger = Logger.getLogger("logsnd");
                sndLogger.info(s);
            }
            else if (cat.startsWith("loggfx"))
            {
                if (gfxLogger == null)
                    gfxLogger = Logger.getLogger("loggfx");
                gfxLogger.info(s);
            }
            else
            {
                Logger.getRootLogger().warn(s);
            }
            // Console.WriteLine(s);
        }

        public void enableEmuLog(bool onoff)
        {
            this.LOGEMU = onoff;
        }

        public void enableGfxLog(bool onoff)
        {
            this.LOGGFX = onoff;
        }

        public void enableGfxExtLog(bool onoff)
        {
            this.LOGGFX_EXT = onoff;
        }

        public void enableHntLog(bool onoff)
        {
            this.LOGHNT = onoff;
        }

        public void enableSndLog(bool onoff)
        {
            this.LOGSND = onoff;
        }

        /*
         * Get code offset from virtual pointer Rev. calc offset in code array
         */

        uint effective(uint ptr)
        {
            if (version < 4 && mem_size == 0x10000)
                return ptr & 0xffff;
            if (ptr >= 131072)
                ptr = ptr - 65536;
            // if (ptr>=mem_size)
            // ms_fatal("Outside memory experience - effective");
            // return 0;
            return ptr;
        }

        /* Standard rand - for equal cross-platform behaviour */

        public void ms_seed(int seed)
        {
            rseed = (uint)seed;
        }

        uint rand_emu()
        {
            rseed = 1103515245 * rseed + 12345;
            return rseed & 0x7fffffff;
        }

        /*
         * Function removed because of garbage collection void ms_freemem(void)
         */
        public bool ms_is_running()
        {
            return running;
        }

        public bool ms_is_magwin()
        {
            if (version == 4)
                return true;
            else
                return false;
        }

        public void ms_stop()
        {
            // Console.WriteLine("STOP reached");
            if (LOGEMU)
                log("logemu", "STOP reached");

            running = false;
        }

        /* zero all registers and flags and load the game */
        bool isInitialised = false;
        public byte ms_init(String name, String hntname, String sndname)
        {
            running = false;

            ms_stop();
            if (name == null)
            {
                if (restart == null)
                {
                    // Console.WriteLine("OOPS. Aborting");
                    if (LOGEMU)
                        log("logemu", "OOPS. Aborting");
                    return 0;
                }
                else
                {
                    Array.Copy(restart, 0, code, 0, undo_size);
                    undo_stat[0] = 0;
                    undo_stat[1] = 0;
                    ms_showpic((byte)0, null, (byte)0);
                }
            }
            else
            {
                //ms_seed((int) (DateTime.Now.Millisecond));
                ms_seed(12345);
                byte[] header = new byte[42];
                var fp = new RandomAccessFile(name, "r");
                if ((fp.ReadUbyteFromFile(header) != 42)
                        || (header.Read32(0) != 0x4d615363))
                {
                    fp.close();
                    return 0;
                }
                if (header.Read32(8) != 42)
                { /* Bad style, but it works for now */
                    fp.close();
                    return 0;
                }

                version = header[13]; /* all Offsets dec 1 */
                var code_size = ((header.Read32(14)));
                string_size = header.Read32(18);
                var string2_size = header.Read32(22);
                var dict_size = header.Read32(26);
                undo_size = header.Read32(34);
                undo_pc = header.Read32(38);

                if ((version < 4) && (code_size < 65536))
                    mem_size = 65536;
                else
                    mem_size = code_size;

                sdd = (dict_size != 0); /*
															 * if (sd) =>
															 * separate dict
															 */

                code = new byte[mem_size];
                tstring = new byte[string_size];
                tstring2 = new byte[string2_size];
                restart = new byte[undo_size];

                if (version > 1)
                    dict = new byte[dict_size];
                if ((code == null) || (tstring == null) || (tstring2 == null)
                        || (restart == null) || (sdd && dict == null))
                {
                    fp.close();
                    return 0;
                }

                undo[0] = new byte[undo_size + 1];
                undo[1] = new byte[undo_size + 1];
                if ((undo[0] == null) || (undo[1] == null))
                {
                    fp.close();
                    return 0;
                }
                if (fp.ReadUbyteFromFile(code, code_size) != code_size)
                {
                    fp.close();
                    return 0;
                }

                if (code_size == 65535)
                    fp.read();

                Array.Copy(code, 0, restart, 0, undo_size); /* fast restarts */
                if (fp.ReadUbyteFromFile(tstring) != string_size)
                {
                    fp.close();
                    return 0;
                }
                if (fp.ReadUbyteFromFile(tstring2) != string2_size)
                {
                    fp.close();
                    return 0;
                }
                if (sdd && fp.ReadUbyteFromFile(dict) != dict_size)
                {
                    fp.close();
                    return 0;
                }

                /* no pointer arithmetic, remember offset */
                var dec = header.Read32(30);
                if (dec >= string_size)
                {
                    decode_src = tstring2;
                    decode_offset = (dec - string_size);
                }
                else
                {
                    decode_src = tstring;
                    decode_offset = dec;
                }
                fp.close();
            }

            // Console.WriteLine("Initializing registers.\n");
            if (LOGEMU)
                log("logemu", "Initializing registers.\n");
            for (var i = 0; i < 8; i++)
            {
                if (dreg[i] == null)
                    dreg[i] = new Register32(this.registerLogger, "D" + i);
                if (areg[i] == null)
                    areg[i] = new Register32(this.registerLogger, "A" + i);
                dreg[i].clearReg();
                areg[i].clearReg();
            }
            allreg = dreg.Concat(areg).ToArray();
            areg[7].Write32(0xfffeU); /*
										 * Stack-pointer, -2 due to MS-DOS
										 * segments
										 */
            pc = 0;
            zflag = nflag = cflag = vflag = false;
            i_count = 0;
            running = true;

            if (name == null)
                return (byte)( isInitialised ? 2 : 1); /* Restarted */

            isInitialised = true;
            // -> Hint file and sound file
            /* Try loading a hint file and a sound file */
            if (version == 4)
            {

                // TODO File exists!
                if (hntname != null)
                {
                    try
                    {
                        this.hints = new MagneticHint(this.magneticio);
                        hints.init_hint(hntname);
                    }
                    catch (IOException e)
                    {
                        // File reading failed
                    }
                }

                /* --> Sound file */


                if (sndname != null)
                {
                    try
                    {
                        this.sound = new MagneticSound();
                        sound.init_snd(sndname);
                    }
                    catch (IOException e)
                    {
                        // File reading failed
                    }
                }
                else
                {
                    sndenabled = false;
                }
            }

            for (var i = 0; i < gfxlist.Length; i++)
            {
                var bytes = Encoding.ASCII.GetBytes(gfxlist[i]);
                if (code.Locate(bytes).Any())
                {
                    this.v4_id = i;
                }
            }

            return 1;
        }

        void save_undo()
        {

            byte[] tmp;
            byte i;
            uint tmp32;

            tmp = new byte[undo_size + 1];

            Array.Copy(undo[0], 0, tmp, 0, undo_size);
            Array.Copy(undo[1], 0, undo[0], 0, undo_size);
            Array.Copy(tmp, 0, undo[0], 0, undo_size);
            tmp = undo[0];
            undo[0] = undo[1];
            undo[1] = tmp;

            for (i = 0; i < 18; i++)
            {
                tmp32 = undo_regs[0][i];
                undo_regs[0][i] = undo_regs[1][i];
                undo_regs[1][i] = tmp32;
            }

            Array.Copy(code, 0, undo[1], 0, undo_size);
            for (i = 0; i < 8; i++)
            {
                undo_regs[1][i] = dreg[i].Read32();
                undo_regs[1][8 + i] = areg[i].Read32();
            }
            undo_regs[1][16] = i_count;
            undo_regs[1][17] = pc;

            undo_stat[0] = undo_stat[1];
            undo_stat[1] = 1;
        }

        byte ms_undo()
        {
            ms_flush();
            if (undo_stat[0] == 0)
                return 0;

            undo_stat[0] = undo_stat[1] = 0;
            Array.Copy(undo[0], 0, code, 0, undo_size);
            for (var i = 0; i < 8; i++)
            {
                dreg[i].Write32(undo_regs[0][i]);
                areg[i].Write32(undo_regs[0][8 + i]);
            }
            i_count = undo_regs[0][16];
            pc = undo_regs[0][17]; /* status flags intentionally omitted */
            return (byte)1;
        }

        public void log_status()
        {
            short j;
            StringBuffer s = new StringBuffer();

            s.append("D0: ");
            for (j = 0; j < 8; j++)
                s.append(" " + dreg[j].Read32().ToString("X"));
            // System.err.println("");
            log("logemu", s.toString());
            s = new StringBuffer();
            s.append("A0:");
            for (j = 0; j < 8; j++)
                s.append(" " + areg[j].Read32().ToString("X"));
            log("logemu", s.toString());
            log("logemu", "PC=" + pc.ToString("X") + " ZCNV=" + (zflag ? "1" : "0")
                    + (cflag ? "1" : "0") + (nflag ? "1" : "0") + (vflag ? "1" : "0") + " - " + i_count
                    + " instructions");
        }

        public void ms_status()
        {
            short j;

            Console.Write("D0: ");
            for (j = 0; j < 8; j++)
                Console.Write(" " + dreg[j].Read32().ToString("X"));
            Console.WriteLine("");
            Console.Write("A0:");
            for (j = 0; j < 8; j++)
                Console.Write(" " + areg[j].Read32().ToString("X"));
            Console.WriteLine("");
            Console.Write("PC=" + pc.ToString("X") + " ZCNV="
                    + (zflag ? "1" : "0") + (cflag ? "1" : "0") + (nflag ? "1" : "0") + (vflag ? "1" : "0") + " - "
                    + i_count + " instructions");
            Console.WriteLine("");
        }

        public long ms_count()
        {
            return i_count;
        }

        /* align register pointer for word/byte accesses */

        byte reg_align(byte size)
        {
            byte bRet = 0;
            if (size == 1)
                bRet = 2;
            if (size == 0)
                bRet = 3;

            return bRet;
        }

        /* [35c4] */

        void byte_out(byte c)
        {

            if (c == 0xff)
            {
                big = true;
                return;
            }
            c &= 0x7f;
            // c&=0x7f;
            if (dreg[3].Read8() != 0)
            {
                if (c == 0x5f)
                    c = 0x20;
                if ((c >= 'a') && (c <= 'z'))
                    c &= 0xdf;
                if (version < 4)
                    ms_statuschar((char)c);
                return;
            }
            if (c == 0x5e)
                c = 0x0a;
            if (c == 0x40)
            {
                if (dreg[2].Read8() != 0)
                    return;
                else
                    c = 0x73;
            }
            if (version < 3 && c == 0x7e)
            {
                lastbyte = 0x7e;
                c = 0x0a;
            }
            if (((c > 0x40) && (c < 0x5b)) || ((c > 0x60) && (c < 0x7b)))
            {
                if (big)
                {
                    c &= 0xdf;
                    big = false;
                }
                if (period)
                    byte_out((byte)0x20);
            }
            period = false;
            // if ((c==0x2e) || (c==0x3f) || (c==0x21) || (c==0x0a)) big=1;
            if (version < 4)
            {
                if ((c == 0x2e) || (c == 0x3f) || (c == 0x21) || (c == 0x0a))
                    big = true;
                else if (c == 0x22)
                    big = false;
            }
            else
            {
                if ((c == 0x20) && (lastbyte == 0x0a))
                    return;
                if ((c == 0x2e) || (c == 0x3f) || (c == 0x21) || (c == 0x0a))
                    big = true;
                else if (c == 0x22)
                    big = false;
            }
            //
            if (((c == 0x20) || (c == 0x0a)) && (c == lastbyte))
                return;
            if (version < 3)
            {
                if (pipe)
                {
                    pipe = false;
                    return;
                }
                if (c == 0x7c)
                {
                    pipe = true;
                    return;
                }
            }
            else
            {
                if (c == 0x7e)
                {
                    c = 0x0a;
                    if (lastbyte != 0x0a)
                        byte_out((byte)0x0a);
                }
            }
            lastbyte = c;
            if (c == 0x5f)
                c = 0x20;
            if ((c == 0x2e) || (c == 0x2c) || (c == 0x3b) || (c == 0x3a)
                    || (c == 0x21) || (c == 0x3f))
            {
                period = true;
            }
            ms_putchar((char)c);
        }

        private void ms_putchar(char p)
        {
            this.magneticio.ms_putchar(p);
        }

        private void ms_statuschar(char p)
        {
            this.magneticio.ms_statuschar(p);
        }

        /* extract addressing mode information [1c6f] */

        void set_info(byte b)
        {
            regnr = (byte)(b & 0x07);
            admode = (AddressMode)((b >> /*TS*/ 3) & 0x07);
            opsize = (byte)(b >> /*TS*/ 6);
        }

        /* read a word and increase pc */

        void read_word()
        {
            var epc = effective(pc);
            byte1 = (byte)code[epc];
            byte2 = (byte)code[epc + 1];
            pc += 2;
        }

        /* get addressing mode and set arg1 [1c84] */

        private class _arg1i
        {
            public uint arg1i { get; set; }
            public bool is_reversible { get; set; }

        }

        enum AddressMode
        {
            VM68K_AM_DATAREG = 0,
            VM68K_AM_ADDRREG = 1,
            VM68K_AM_INDIR = 2,
            VM68K_AM_POSTINC = 3,
            VM68K_AM_PREDEC = 4,
            VM68K_AM_DISP16 = 5,
            VM68K_AM_INDEX = 6,
            VM68K_AM_EXT = 7,
        }

        _arg1i set_arg1()
        {
            var result = new _arg1i
            {
                is_reversible = true,
            };

            switch (admode)
            {
                case AddressMode.VM68K_AM_DATAREG:
                    acc1 = dreg[regnr].bytes; /* Dx */
                    arg1 = reg_align(opsize);
                    result.is_reversible = false;
                    if (LOGEMU)
                    {
                        log("logemu", " d" + regnr);
                    }
                    break;
                case AddressMode.VM68K_AM_ADDRREG:
                    acc1 = areg[regnr].bytes;
                    arg1 = reg_align(opsize);
                    result.is_reversible = false;
                    if (LOGEMU)
                    {
                        log("logemu", " a" + regnr);
                    }
                    break;
                case AddressMode.VM68K_AM_INDIR:
                    result.arg1i = areg[regnr].Read32(); /* (Ax) */
                    if (LOGEMU)
                    {
                        log("logemu", " (a" + regnr + ")");
                    }
                    break;
                case AddressMode.VM68K_AM_POSTINC:
                    result.arg1i = areg[regnr].Read32(); /* (Ax)+ */
                    areg[regnr].Write32(areg[regnr].Read32() + (uint)(1 << opsize));
                    if (LOGEMU)
                    {
                        log("logemu", " (a" + regnr + ")+");
                    }
                    break;
                case AddressMode.VM68K_AM_PREDEC:
                    areg[regnr].Write32(areg[regnr].Read32() - (uint)(1 << opsize));
                    result.arg1i = areg[regnr].Read32(); /* -(Ax) */
                    if (LOGEMU)
                    {
                        log("logemu", " -(a" + regnr + ")");
                    }
                    break;
                case AddressMode.VM68K_AM_DISP16:
                    var i = (short)code.Read16(effective(pc));
                    result.arg1i = (uint)(areg[regnr].Read32() + i);
                    pc += 2; /* offset.w(Ax) */
                    if (LOGEMU)
                    {
                        log("logemu", " " + i + "(a" + regnr + ")");
                    }
                    break;
                case AddressMode.VM68K_AM_INDEX:
                    byte[] tmp = new byte[2];
                    tmp[0] = byte1;
                    tmp[1] = byte2;
                    read_word(); /* offset.b(Ax, Dx/Ax) [1d1c] */
                    StringBuffer s = new StringBuffer();
                    if (LOGEMU)
                        s.append(" " + (int)byte2 + "(a" + regnr + ",");

                    result.arg1i = (uint)(areg[regnr].Read32() + (sbyte)byte2);
                    if (LOGEMU)
                    {
                        if ((byte1 >> /*TS*/ 4) > 8)
                            s.append("a" + ((byte1 >> /*TS*/ 4) - 8));
                        else
                            s.append("d" + (byte1 >> /*TS*/ 4));
                    }
                    if ((byte1 & 0x08) != 0)
                    {
                        if (LOGEMU)
                        {
                            log("logemu", s.append(".l)").toString());
                        }
                        int ix1 = (int)allreg[byte1 >> /*TS*/ 4].Read32();
                        result.arg1i += (uint)ix1;
                    }
                    else
                    {
                        if (LOGEMU)
                        {
                            log("logemu", s.append(".w)").toString());
                        }
                        short sx1 = (short)allreg[byte1 >> /*TS*/ 4].Read16();
                        result.arg1i += (uint)sx1;
                    }
                    byte1 = tmp[0];
                    byte2 = tmp[1];
                    break;
                case AddressMode.VM68K_AM_EXT: /* specials */
                    switch (regnr)
                    {

                        case 0:
                            result.arg1i = code.Read16(effective(pc)); /* $xxxx.W */
                            pc += 2;
                            if (LOGEMU)
                                log("logemu", result.arg1i + ".w");
                            break;
                        case 1:
                            result.arg1i = code.Read32(effective(pc)); /* $xxxx */
                            pc += 4;
                            if (LOGEMU)
                                log("logemu", "" + result.arg1i);
                            break;
                        case 2:
                            result.arg1i = (uint)((short)code.Read16(effective(pc)) + pc); /* $xxxx(PC) */
                            pc += 2;
                            if (LOGEMU)
                                log("logemu", " " + result.arg1i + "(pc)");
                            break;
                        case 3:
                            var l1c = code[effective(pc)]; /* $xx(PC,A/Dx) */
                            if (LOGEMU)
                                log("logemu", " ???2");
                            if ((l1c & 0x08) != 0)
                                result.arg1i = (uint)(pc + (int)allreg[l1c >> /*TS*/ 4].Read32());
                            else
                                result.arg1i = (uint)(pc + (short)allreg[l1c >> /*TS*/ 4].Read16());
                            l1c = code[effective(pc) + 1];
                            pc += 2;
                            result.arg1i += (uint)((sbyte)l1c);
                            break;
                        case 4:
                            result.arg1i = pc; /* #$xxxx */
                            if (opsize == 0)
                                result.arg1i += 1;
                            pc += 2;
                            if (opsize == 2)
                                pc += 2;
                            if (LOGEMU)
                                log("logemu", " #" + result.arg1i);
                            break;
                    }
                    break;
            }
            if (result.is_reversible)
            {
                acc1 = code;
                arg1 = effective(result.arg1i);
            }
            return result;
        }

        /* get addressing mode and set arg2 [1bc5] */

        void set_arg2_nosize(bool use_dx, byte b)
        { /* c=0 corresponds to use_dx */

            if (use_dx)
            {
                acc2 = dreg[((b & 0x0e) << 1) / 4].bytes; /*
														 * 4 is logical byte
														 * count of reg.
														 */
                if ((((b & 0x0e) << 1) % 4) != 0)
                    ms_fatal("reg align is not null - set_arg2");
            }
            else
            {
                acc2 = areg[((b & 0x0e) << 1) / 4].bytes;
                if ((((b & 0x0e) << 1) % 4) != 0)
                    ms_fatal("reg align is not null - set_arg2");
            }
            // VORSICHT: UNTERSCHIED ZU EMU.C
            // arg2= reg_align(opsize);
            arg2 = 0;

        }

        void set_arg2(bool use_dx, byte b)
        { /* c=0 corresponds to use_dx */

            set_arg2_nosize(use_dx, b);
            arg2 = reg_align(opsize);
        }

        /* [1b9e] */

        void swap_args()
        {
            /*
             * if (i_count >= 55311) { a = acc1[arg1]; b = acc1[arg1+1]; c =
             * acc1[arg1+2]; d = acc1[arg1+3]; e = acc2[arg2]; f = acc2[arg2+1]; g =
             * acc2[arg2+2]; h = acc2[arg2+3]; }
             */
            var tmp = acc1;
            var tmp2 = arg1;
            acc1 = acc2;
            arg1 = arg2;
            acc2 = tmp;
            arg2 = tmp2;
        }

        /* [1cdc] */

        void push(uint c)
        {
            areg[7].Write32(areg[7].Read32() - 4);
            code.Write32(effective(areg[7].Read32()), c);
        }

        /* [1cd1] */

        uint pop()
        {
            var tmp = areg[7].Read32();
            var tmp2 = effective(tmp);
            var c = code.Read32(tmp2);
            // Fixes for Magnetic Windows games
            if (c >= 131072)
                c = c - 65536;
            if (c >= mem_size)
                c = c - 65536;

            areg[7].Write32(areg[7].Read32() + 4);
            return c;
        }

        /* check addressing mode and get argument [2e85] */
        void get_arg()
        {
            if (LOGEMU)
            {
                log("logemu", " " + pc);
            }
            set_info(byte2);
            arg2 = effective(pc);
            acc2 = code;
            pc += 2;
            if (opsize == 2)
                pc += 2;
            if (opsize == 0)
                arg2 += 1;
            set_arg1();
        }

        void set_flags()
        {
            zflag = nflag = false;
            switch (opsize)
            {

                case 0:
                    nflag = (acc1.Read8(arg1) > 127);
                    zflag = (acc1.Read8(arg1) == 0);
                    break;
                case 1:
                    var i = acc1.Read16(arg1); // hier geht's schief
                    zflag = (i == 0);
                    nflag = (i >> /*TS*/ 15 > 0);
                    break;
                case 2:
                    var j = acc1.Read32(arg1);
                    zflag = (j == 0);
                    nflag = (j >> /*TS*/ 31 > 0);
                    break;
            }
        }

        /* [263a] */

        bool condition(byte b)
        {
            var bRes = false;
            switch (b & 0x0f)
            {
                case 0:
                    return true;
                case 1:
                    return false;
                case 2:
                    return (zflag | cflag) ^ true;
                case 3:
                    return zflag | cflag;
                case 4:
                    return cflag ^ true;
                case 5:
                    return cflag;
                case 6:
                    return zflag ^ true;
                case 7:
                    return zflag;
                case 8:
                    return vflag ^ true;
                case 9:
                    return vflag;
                case 10:
                case 12:
                    return nflag ^ true;
                case 11:
                case 13:
                    return nflag;
                case 14:
                    return (zflag | nflag) ^ true;
                case 15:
                    return zflag | nflag;
            }
            return bRes;
        }

        /* [26dc] */

        void branch(byte b)
        {
            if (b == 0)
                pc = (uint)(pc + (short)code.Read16(effective(pc)));
            else
                pc = (uint)(pc + (sbyte)b); /* changed from short to byte for sign */
            if (LOGEMU)
            {
                log("logemu", " " + pc);
            }
        }

        /* [2869] */

        void do_add(bool adda)
        {
            if (adda)
            {
                if (opsize == 0)
                {
                    acc1.Write32(arg1, (uint)(acc1.Read32(arg1) + (sbyte)acc2.Read8(arg2)));
                }
                if (opsize == 1)
                {
                    acc1.Write32(arg1, (uint)(acc1.Read32(arg1) + (short)acc2.Read16(arg2)));
                }
                if (opsize == 2)
                {
                    acc1.Write32(arg1, (uint)(acc1.Read32(arg1) + (int)acc2.Read32(arg2)));
                }
            }
            else
            {
                cflag = false;
                if (opsize == 0)
                {
                    acc1.Write8(arg1, (byte)((acc1.Read8(arg1) + acc2.Read8(arg2)) % 256));
                    if (acc2.Read8(arg2) > acc1.Read8(arg1))
                        cflag = true;
                }
                if (opsize == 1)
                {
                    acc1.Write16(arg1, (ushort)(acc1.Read16(arg1) + acc2.Read16(arg2)));
                    if (acc2.Read16(arg2) > acc1.Read16(arg1))
                        cflag = true;
                }
                if (opsize == 2)
                {
                    acc1.Write32(arg1, acc1.Read32(arg1) + acc2.Read32(arg2));
                    if (acc2.Read32(arg2) > acc1.Read32(arg1))
                        cflag = true;
                }
                if ((version < 3) || (quick_flag == false))
                { /* Corruption onwards */
                    vflag = false;
                    set_flags();
                }
            }
        }

        /* [2923] */

        void do_sub(bool suba)
        {
            if (suba)
            {
                if (opsize == 0)
                    acc1.Write32(arg1, (uint)(acc1.Read32(arg1) - (sbyte)acc2.Read8(arg2)));
                if (opsize == 1)
                    acc1.Write32(arg1, (uint)(acc1.Read32(arg1) - (short)acc2.Read16(arg2)));
                if (opsize == 2)
                    acc1.Write32(arg1, (uint)(acc1.Read32(arg1) - (int)acc2.Read32(arg2)));
            }
            else
            {
                cflag = false;
                if (opsize == 0)
                {
                    if (acc2.Read8(arg2) > acc1.Read8(arg1))
                    {
                        cflag = true;
                        acc1.Write8(arg1, (byte)(acc1.Read8(arg1) - acc2.Read8(arg2) + 256));
                    }
                    else
                    {
                        acc1.Write8(arg1, (byte)(acc1.Read8(arg1) - acc2.Read8(arg2)));
                    }
                }
                if (opsize == 1)
                {
                    int a = acc2.Read16(arg2);
                    int b = acc1.Read16(arg1);
                    cflag = (a > b);
                    a = acc1.Read16(arg1);
                    b = acc2.Read16(arg2);
                    acc1.Write16(arg1, (ushort)(a - b));
                }
                if (opsize == 2)
                {
                    int l1 = (int)acc2.Read32(arg2);
                    int l2 = (int)acc1.Read32(arg1);
                    var l3 = l1 < 0 ? int.MaxValue : l1;
                    var l4 = l2 < 0 ? int.MaxValue : l2;
                    cflag = l3 > l4;
                    var lw = l2 - l1;
                    acc1.Write32(arg1, (uint)lw);
                }
                if ((version < 3) || (quick_flag == false))
                { /* Corruption onwards */
                    vflag = false;
                    set_flags();
                }
            }
        }

        /* [283b] */

        void do_eor()
        {
            if (opsize == 0)
                acc1.Write8(arg1, (byte)(acc1.Read8(arg1) ^ acc2.Read8(arg2)));
            if (opsize == 1)
                acc1.Write16(arg1, (ushort)(acc1.Read16(arg1) ^ acc2.Read16(arg2)));
            if (opsize == 2)
                acc1.Write32(arg1, acc1.Read32(arg1) ^ acc2.Read32(arg2));
            cflag = vflag = false;
            set_flags();
        }

        /* [280d] */

        void do_and()
        {
            if (opsize == 0)
                acc1.Write8(arg1, (byte)(acc1.Read8(arg1) & acc2.Read8(arg2)));
            if (opsize == 1)
                acc1.Write16(arg1, (ushort)(acc1.Read16(arg1) & acc2.Read16(arg2)));
            if (opsize == 2)
                acc1.Write32(arg1, acc1.Read32(arg1) & acc2.Read32(arg2));
            cflag = vflag = false;
            set_flags();
        }

        /* [27df] */

        void do_or()
        {
            if (opsize == 0)
                acc1.Write8(arg1, (byte)(acc1.Read8(arg1) | acc2.Read8(arg2)));
            if (opsize == 1)
                acc1.Write16(arg1, (ushort)(acc1.Read16(arg1) | acc2.Read16(arg2)));
            if (opsize == 2)
                acc1.Write32(arg1, acc1.Read32(arg1) | acc2.Read32(arg2));
            cflag = vflag = false;
            set_flags(); /* [1c2b] */
        }

        /* [289f] */

        void do_cmp()
        {
            var tmp = acc1;
            var tmp2 = arg1;
            byte[] tmparg = new byte[4];
            if (acc1 != code)
            {
                tmparg[0] = acc1.Read8(arg1);
                tmparg[1] = (arg1 < 3) ? (byte)acc1.Read8(arg1 + 1) : (byte)0x00;
                tmparg[2] = (arg1 < 2) ? (byte)acc1.Read8(arg1 + 2) : (byte)0x00;
                tmparg[3] = (arg1 < 1) ? (byte)acc1.Read8(arg1 + 3) : (byte)0x00;
            }
            else
            {
                tmparg[0] = acc1.Read8(arg1);
                tmparg[1] = acc1.Read8(arg1 + 1);
                tmparg[2] = acc1.Read8(arg1 + 2);
                tmparg[3] = acc1.Read8(arg1 + 3);
            }
            acc1 = tmparg;
            arg1 = 0;
            quick_flag = false;
            do_sub(false);
            acc1 = tmp;
            arg1 = tmp2;
        }

        /* [2973] */

        void do_move()
        {

            if (opsize == 0)
                acc1.Write8(arg1, acc2.Read8(arg2));
            if (opsize == 1)
                acc1.Write16(arg1, acc2.Read16(arg2));
            if (opsize == 2)
                acc1.Write32(arg1, acc2.Read32(arg2));
            if (version < 2 || admode != AddressMode.VM68K_AM_ADDRREG)
            { /*
											 * Jinxter: no flags if destination
											 * Ax
											 */
                cflag = vflag = false;
                set_flags();
            }
        }

        byte do_btst(byte a)
        {

            a &= (admode != AddressMode.VM68K_AM_DATAREG) ? (byte)0x7 : (byte)0x1f;
            while (admode == AddressMode.VM68K_AM_DATAREG && a >= 8)
            {
                a -= 8;
                if (arg1 == 0)
                    ms_fatal("Violation of reg bounds - do_btst");
                arg1 -= 1;
                /*
                 * PROBLEM: Orginal: arg1-=1; Does this work? What happens if arg1 =
                 * 0-offset --> get reg below?
                 */
            }
            zflag = false;
            if ((acc1.Read8(arg1) & (1 << a)) == 0)
                zflag = true;
            return a;
        }

        /* bit operation entry point [307c] */
        void do_bop(byte b, byte a)
        {

            if (LOGEMU)
            {
                log("logemu", "bop (" + b + "," + a + ") ");
            }
            b = (byte)(b & 0xc0);
            a = do_btst(a);
            if (LOGEMU)
            {
                if (b == 0x00)
                    log("logemu", "no bop???");
            }
            if (b == 0x40)
            {
                acc1.Write8(arg1, (byte)(acc1.Read8(arg1) ^ (1 << a)));
                if (LOGEMU)
                {
                    log("logemu", "bchg");
                }
            } /* bchg */
            if (b == 0x80)
            {
                acc1.Write8(arg1,
                        (byte)(acc1.Read8(arg1) & ((1 << a) ^ 0xff)));
                if (LOGEMU)
                {
                    log("logemu", "bclr");
                }
            } /* bclr */
            if (b == 0xc0)
            {
                acc1.Write8(arg1, (byte)(acc1.Read8(arg1) | (1 << a)));
                if (LOGEMU)
                {
                    log("logemu", "bset");
                }
            } /* bset */
        }

        void check_btst()
        {

            if (LOGEMU)
            {
                log("logemu", "btst");
            }
            set_info((byte)(byte2 & 0x3f));
            set_arg1();
            set_arg2(true, byte1);
            do_bop(byte2, acc2.Read8(arg2));
        }

        void check_lea()
        {

            if (LOGEMU)
            {
                log("logemu", "lea");
            }
            if ((byte2 & 0xc0) == 0xc0)
            {
                set_info(byte2);
                opsize = 2;
                var arg = set_arg1();
                set_arg2(false, byte1);
                acc2.Write16(arg2, 5); //JM ???
                if (arg.is_reversible)
                    acc2.Write32(arg2, arg.arg1i);
                else
                    ms_fatal("illegal addressing mode for LEA - check_lea");
            }
            else
            {
                ms_fatal("unimplemented instruction CHK");
            }
        }

        /* [33cc] */

        void check_movem()
        {
            if (LOGEMU)
            {
                log("logemu", "movem");
            }
            set_info((byte)(byte2 - 0x40));
            read_word();
            for (var l1c = 0; l1c < 8; l1c++)
            {
                if ((byte2 & 1 << l1c) != 0)
                {
                    set_arg1();
                    if (opsize == 2)
                        acc1.Write32(arg1, areg[7 - l1c].Read32());
                    if (opsize == 1)
                        acc1.Write16(arg1, areg[7 - l1c].Read16());
                }
            }
            for (var l1c = 0; l1c < 8; l1c++)
            {
                if ((byte1 & 1 << l1c) != 0)
                {
                    set_arg1();
                    if (opsize == 2)
                        acc1.Write32(arg1, dreg[7 - l1c].Read32());
                    if (opsize == 1)
                        acc1.Write16(arg1, dreg[7 - l1c].Read16());
                }
            }
        }

        /* [3357] */

        void check_movem2()
        {
            if (LOGEMU)
            {
                log("logemu", "movem (2)");
            }
            set_info((byte)(byte2 - 0x40));
            read_word();
            for (var l1c = 0; l1c < 8; l1c++)
            {
                if ((byte2 & 1 << l1c) != 0)
                {
                    set_arg1();
                    if (opsize == 2)
                        dreg[l1c].Write32(acc1.Read32(arg1));
                    if (opsize == 1)
                        dreg[l1c].Write16(acc1.Read16(arg1));
                }
            }
            for (var l1c = 0; l1c < 8; l1c++)
            {
                if ((byte1 & 1 << l1c) != 0)
                {
                    set_arg1();
                    if (opsize == 2)
                        areg[l1c].Write32(acc1.Read32(arg1));
                    if (opsize == 1)
                        areg[l1c].Write16(acc1.Read16(arg1));
                }
            }
        }

        /* [30e4] in Jinxter, ~540 lines of 6510 spaghetti-code */
        /* The mother of all bugs, but hey - no gotos used :-) */

        void dict_lookup()
        {
            ushort output_bak, output2, obj_adj, adjlist_bak;
            byte c;
            bool restart = false, accept = false;

            var dtab = areg[5].Read16(); /* used by version>0 */
            var output = areg[2].Read16();
            areg[5].Write16(areg[6].Read16());
            var doff = areg[3].Read16();
            var adjlist = areg[0].Read16();

            var bank = dreg[6].Read8(); /* l2d */
            byte flag = 0; /* l2c */
            ushort word = 0; /* l26 */
            byte matchlen = 0; /* l2e */
            byte longest = 0; /* 30e2 */
            dreg[0].Write16((ushort)0); /* apostroph */

        goon: while ((c = sdd ? dict[doff] : code[effective(doff)]) != 0x81)
            {
                if (c >= 0x80)
                {
                    if (c == 0x82)
                    {
                        flag = matchlen = 0;
                        word = 0;
                        areg[6].Write16(areg[5].Read16());
                        bank++;
                        doff++;
                        continue;
                    }
                    var c3 = c;
                    c &= 0x5f;
                    var c2 = (byte)(code[effective(areg[6].Read16())] & 0x5f);
                    if (c2 == c)
                    {
                        areg[6].Write16(((ushort)(areg[6].Read16() + 1)));
                        c = code[effective(areg[6].Read16())];
                        if ((c == 0) || (c == 0x20) || (c == 0x27)
                                || (version == 0 && (matchlen > 6)))
                        {
                            if (c == 0x27)
                            {
                                areg[6].Write16((ushort)(areg[6].Read16() + 1));
                                dreg[0].Write16((ushort)((uint)0x200 + code[effective(areg[6].Read16())]));
                            }
                            if ((version < 4) || (c3 != 0xa0))
                                accept = true;
                        }
                        else
                            restart = true;
                    }
                    else if (version == 0 && matchlen > 6 && c2 == 0)
                        accept = true;
                    else
                        restart = true;
                }
                else
                {
                    c &= 0x5f;
                    var c2 = (byte)(code[effective(areg[6].Read16())] & 0x5f);
                    if ((c2 == c && c != 0)
                            || (version != 0 && c2 == 0 && (c == 0x5f)))
                    {
                        if (version != 0 && c2 == 0 && (c == 0x5f))
                            flag = 0x80;
                        matchlen++;
                        areg[6].Write16((ushort)(areg[6].Read16() + 1));
                        doff++;
                    }
                    else if (version == 0 && matchlen > 6 && c2 == 0)
                        accept = true;
                    else
                        restart = true;
                }
                if (accept)
                {
                    code[effective(areg[2].Read16())] = (version != 0) ? flag
                            : (byte)0;
                    code[effective(areg[2].Read16()) + 1] = bank;
                    code.Write16(effective(areg[2].Read16()) + 2, word);
                    areg[2].Write16((ushort)(areg[2].Read16() + 4));
                    if (matchlen >= longest)
                        longest = matchlen;
                    restart = true;
                    accept = false;
                }
                if (restart)
                {
                    areg[6].Write16(areg[5].Read16());
                    flag = matchlen = 0;
                    word++;
                    if (sdd)
                        while (dict[doff++] < 0x80)
                            ;
                    else
                        while (code[effective(doff++)] < 0x80)
                            ;
                    restart = false;
                }
            }
            code.Write16(effective(areg[2].Read16()), 0xffff);

            if (version != 0)
            { /* version > 0 */
                output_bak = output; /* check synonyms */
                c = code[effective(output) + 1];
                while (c != 0xff)
                {
                    ushort tmp16;
                    if (c == 0x0b)
                    {
                        if (sdd)
                            tmp16 = dict.Read16(dtab + (uint)(code.Read16(effective((uint)(output + 2))) * 2));
                        else
                            tmp16 = code.Read16(effective(dtab + (uint)(code.Read16(effective((uint)(output + 2))) * 2)));
                        code[effective(output) + 1] = (byte)(tmp16 & 0x1f);
                        code.Write16(effective((uint)(output + 2)), (ushort)(tmp16 >> /*TS*/ 5));
                    }
                    output += 4;
                    c = code[effective(output) + 1];
                }
                output = output_bak;
            }

            /* l22 = output2, l1e = adjlist, l20 = obj_adj, l26 = word, l2f = c2 */
            /* l1c = adjlist_bak, 333C = i, l2d = bank, l2c = flag, l30e3 = flag2 */

            dreg[1].Write16(0); /* D1.W=0 [32B5] */
            byte flag2 = 0;
            output_bak = output;
            output2 = output;
            bank = (byte)code[effective(output2) + 1];
            while (bank != 0xff)
            {
                obj_adj = areg[1].Read16(); /*
											 * A1.W - obj_adj, ie. adjs for this
											 * word
											 */
                dreg[1].Write8(0); /* D1.B=0 */
                flag = code[effective(output2)]; /* flag */
                word = code.Read16(effective((uint)(output2 + 2))); /* wordnumber */
                output2 += 4; /* next match */
                if ((code.Read16(effective(obj_adj)) != 0) && (bank == 6))
                { /*
																			 * Any
																			 * adjectives?
																			 */
                    var i = word;
                    if (i != 0)
                    { /* Find list of valid adjs */
                        do
                        {
                            while (code[effective(adjlist++)] != 0)
                                ;
                        } while (--i > 0);
                    }
                    adjlist_bak = adjlist;
                    ushort tmp16;
                    do
                    {
                        adjlist = adjlist_bak;
                        var c2 = code[effective(obj_adj) + 1]; /* given adjective */
                        tmp16 = code.Read16(effective(obj_adj));
                        if (tmp16 != 0)
                        {
                            obj_adj += 2;
                            c = code[effective(adjlist++)];
                            while ((c != 0) && (c - 3 != c2))
                            {
                                c = code[effective(adjlist++)];
                            }
                            ;
                            if (c - 3 != c2)
                                dreg[1].Write8(1); /* invalid adjective */
                        }
                    } while (tmp16 != 0 && dreg[1].Read8() == 0);
                    adjlist = areg[0].Read16();
                }
                if (dreg[1].Read8() == 0)
                { /* invalid_flag */
                    flag2 |= flag;
                    code[effective(output)] = flag2;
                    code[effective(output) + 1] = bank;
                    code.Write16(effective((uint)(output + 2)), word);
                    output += 4;
                }
                bank = code[effective(output2) + 1];
            }
            areg[2].Write16(output);
            output = output_bak;

            if ((flag2 & 0x80) != 0)
            {
                var tmp16 = output;
                output -= 4;
                do
                {
                    output += 4;
                    c = code[effective(output)];
                } while ((c & 0x80) == 0);
                code.Write32(effective(tmp16),
                        code.Read32(effective(output)) & 0x7fffffff);
                areg[2].Write32((uint)(tmp16 + 4));
                if (longest > 1)
                {
                    areg[5].Write16((ushort)(areg[5].Read16() + longest - 2));
                }
            }
            areg[6].Write16((ushort)(areg[5].Read16() + 1));
        }



        /* A0=findproperties(D0) [2b86], properties_ptr=[2b78] A0FE */

        void do_findprop()
        {
            ushort tmp;

            if ((version > 2) && ((dreg[0].Read16() & 0x3fff) > fp_size))
            {
                uint tmp1 = (uint)(dreg[0].Read16() & 0x3fff);
                uint tmp2 = ((fp_size - tmp1) < 0) ? (fp_size - tmp1 + 65536)
                        : (fp_size - tmp1);
                uint tmp3 = tmp2 ^ 0xffff;
                tmp = (ushort)(tmp3 << 1); // JM ???
                                           // tmp=(((fp_size-(read_reg(0,1) & 0x3fff)) ^ 0xffff) << 1);
                tmp += fp_tab;
                //Console.WriteLine( "Adress: "+tmp);
                tmp = code.Read16(effective(tmp));
            }
            else
            {
                if (version < 2)
                    dreg[0].Write32(dreg[0].Read32() & 0x7fff);
                else
                    dreg[0].Write16((ushort)(dreg[0].Read16() & 0x7fff));
                tmp = dreg[0].Read16();
            }
            tmp &= 0x3fff;
            //Console.WriteLine( "Adress: "+tmp);
            areg[0].Write32((uint)(tmp * 14 + properties));
        }

        void write_string()
        {
            byte c, b;
            byte mask;
            uint offset;

            if (cflag == false)
            { /* new string */
                var ptr = dreg[0].Read16();
                if (ptr == 0)
                    offset = 0;
                else
                {
                    offset = decode_src.Read16((uint)(decode_offset + 0x100 + 2 * ptr));
                    if (decode_src.Read16(0x100) != 0)
                    {
                        if (ptr >= decode_src.Read16(decode_offset + 0x100))
                            offset += string_size;
                    }
                }
                mask = 1;
            }
            else
            {
                offset = offset_bak;
                mask = mask_bak;
            }
            do
            {
                c = 0;
                while (c < 0x80)
                {
                    if (offset >= string_size)
                        b = tstring2[offset - string_size];
                    else
                        b = tstring[offset];
                    if ((b & mask) != 0)
                        c = decode_src[decode_offset + 0x80 + c];
                    else
                        c = decode_src[decode_offset + c];
                    mask <<= 1;
                    if (mask == 0)
                    {
                        mask = 1;
                        offset++;
                    }
                }
                c &= 0x7f;
                if ((c != 0 && ((c != 0x40) || (lastbyte != 0x20))))
                    byte_out(c);
            } while ((c != 0 && ((c != 0x40) || (lastbyte != 0x20))));
            cflag = (c != 0);
            if (c != 0)
            {
                offset_bak = offset;
                mask_bak = mask;
            }
        }

        void output_text(char[] text)
        {
            for (var i = 0; i < text.Length; i++)
                ms_putchar(text[i]);
        }

        private char ms_getchar(int p)
        {
            return this.magneticio.ms_getchar(p);
        }

        private void ms_flush()
        {
            this.magneticio.ms_flush();
        }

        void do_line_a()
        {
            if ((byte2 < 0xdd) || (version < 4 && byte2 < 0xe4)
                    || (version < 2 && byte2 < 0xed))
            {
                // System.err.println("\nDO_LINE_A - special io ");
                ms_flush(); /* flush output-buffer */
                rand_emu(); /* Increase game randomness */
                var l1c = (byte)ms_getchar(1); /* 0 means UNDO */
                if (l1c == 1)
                    return;
                if (l1c != 0)
                    dreg[1].Write32(l1c); /* d1=getkey() */
                else
                {
                    if ((l1c = ms_undo()) != 0)
                        output_text(undo_ok);
                    else
                        output_text(undo_fail);
                    if (l1c == 0x00)
                        dreg[1].Write32('\n');
                }
            }
            else
            {

                // System.err.println("\nDO_LINE_A - Opcode: "+ (byte2-0xdd));
                if (LOGEMU)
                    log("logemu", "DO_LINE_A - Opcode: " + (byte2 - 0xdd));
                switch (byte2 - 0xdd)
                {

                    case 0: /* A0DD - Won't probably be needed at all */
                        break;
                    case 1: /* A0DE */
                        dreg[1].Write8(1); /* Should remove the manual check */
                        break;
                    case 2: /* A0DF */

                        uint a1reg = areg[1].Read32();

                        int dtype = code[a1reg + 2];

                        //ms_status(); // 0x3A4C
                        if (LOGEMU)
                        {
                            StringBuffer df_dump = new StringBuffer();
                            for (int u = 0; u <= 15; u++)
                                df_dump.append(((code[a1reg + u] <= 15) ? "0" : "") + code[a1reg + u].ToString("X").toUpperCase() + " ");
                            for (int u = 3; u <= 15; u++)
                                df_dump.append((code[a1reg + u] > 30 && code[a1reg + u] < 128) ? "" + code[a1reg + u] : "_");
                            log("logemu", "A0DF: " + df_dump);
                        }
                        //					for (int u=0;u<=15;u++)
                        //						Console.Write(((code[a1reg+u]<=15)?"0":"")+(Integer.toHexString(code[a1reg+u])).toUpperCase()+" ");
                        //					for (int u=3;u<=15;u++)
                        //						Console.Write((code[a1reg+u]>30 && code[a1reg+u]<128)?""+code[a1reg+u]:"_");
                        //					Console.WriteLine();  

                        switch (dtype)
                        {
                            case 0x07: // Picture
                                String sPicName = "";
                                int nameLen = code[a1reg] - 2;
                                byte[] picName = new byte[nameLen];
                                if (nameLen > 0)
                                {
                                    Array.Copy(code, a1reg + 3, picName, 0, nameLen);
                                    sPicName = Encoding.ASCII.GetString(picName);
                                }
                                if (LOGGFX)
                                {
                                    log("loggfx", "PICTURE IS " + sPicName);
                                }
                                /* gfx mode = normal, df is not called if graphics are off */
                                ms_showpic((int)(a1reg + 3), sPicName, (byte)2);
                                break;
                            case 0x0A:// Open window commands
                                switch (code[a1reg + 3])
                                {
                                    case 0: /* Carried items */
                                        output_text(not_supported);
                                        break;
                                    case 1: /* Room items */
                                        output_text(not_supported);
                                        break;
                                    case 2: /* Map */
                                        output_text(not_supported);
                                        break;
                                    case 3: /* Compass */
                                        output_text(not_supported);
                                        break;
                                    case 4: /* Help/Hints */
                                        if (hints.HasHints())
                                        {
                                            hints.show_hints_text();
                                        }
                                        else
                                            output_text(no_hints);
                                        break;
                                }
                                break;
                            case 0x0D: // Music
                                switch (code[a1reg + 3])
                                {
                                    case 0: /* stop music */
                                        ms_playmusic(null);
                                        break;
                                    default: /* play music */
                                        String sSndName = "";
                                        int nameLenMusic = code[a1reg] - 2;
                                        byte[] sndName = new byte[nameLenMusic];
                                        if (nameLenMusic > 0)
                                        {
                                            Array.Copy(code, a1reg + 3, sndName, 0,
                                                            nameLenMusic);
                                            sSndName = Encoding.ASCII.GetString(sndName);
                                        }
                                        if (LOGSND)
                                        {
                                            log("logsnd", "MUSIC IS " + Encoding.ASCII.GetString(sndName));
                                        }
                                        if (sndenabled)
                                            ms_playmusic(this.sound.GetSound(sSndName));
                                        break;
                                }
                                break;
                        }
                        break;
                    case 3: /* A0E0 */
                            /* printf("A0E0 stubbed\n"); */
                        break;
                    case 4: /* A0E1 Read from keyboard to (A1), status in D1 (0 for ok) */
                        dreg[1].Write16(0);
                        ms_flush();
                        rand_emu();
                        var tmp32 = areg[1].Read32();
                        var offset = effective(tmp32);
                        var tmp16 = 0u;
                        byte readchar;
                        do
                        {
                            readchar = (byte)ms_getchar(1);
                            if (readchar == 0)
                            {
                                if ((readchar = ms_undo()) != 0)
                                    output_text(undo_ok);
                                else
                                    output_text(undo_fail);
                                if (readchar == 0x00)
                                {
                                    tmp16 = 1;
                                    readchar = (byte)'\n';
                                    output_text("\n>".toCharArray());
                                }
                                else
                                {
                                    ms_putchar('\n');
                                    return;
                                }
                            }
                            else
                            {
                                if (readchar == 1)
                                    return;
                                code[offset + (tmp16++)] = readchar;
                                if (LOGGFX)
                                {
                                    log("loggfx", "" + readchar);
                                }
                            }
                        } while ((readchar != '\n') && (tmp16 < 256));
                        areg[1].Write32(tmp32 + (tmp16 - 1));
                        if ((tmp16 != 256) && (tmp16 != 1))
                        {
                            dreg[1].Write16(0);
                        }
                        else
                            dreg[1].Write16(1);
                        // Console.WriteLine(i_count);
                        // ms_status();
                        break;
                    case 5: /* A0E2 */
                        Console.WriteLine("Reached A0E2.");
                        /* printf("A0E2 stubbed\n"); */
                        break;
                    case 6: /* A0E3 */
                            /* printf("\nMoves: %u\n",read_reg(0,1)); */
                        if (dreg[1].Read32() == 0)
                        {
                            // Console.WriteLine( "Graphics off");
                            if ((version < 4) || dreg[6].Read32() == 0)
                                ms_showpic(0, null, 0);
                        }
                        break;

                    case 7: /* A0E4 sp+=4, RTS */
                        areg[7].Write16((ushort)(areg[7].Read16() + 4));
                        pc = pop();
                        break;

                    case 8: /* A0E5 set z, RTS */
                    case 9: /* A0E6 clear z, RTS */
                        pc = pop();
                        zflag = (byte2 == 0xe5);
                        break;

                    case 10: /* A0E7 set z */
                        zflag = true;
                        break;

                    case 11: /* A0E8 clear z */
                        zflag = false;
                        break;

                    case 12: /* A0E9 [3083 - j] */
                        var ptr12_a0 = areg[0].Read16();
                        var ptr12_a1 = areg[1].Read16();
                        byte dictchar12;
                        do
                        {
                            dictchar12 = dict[ptr12_a1++];
                            code[effective(ptr12_a0++)] = dictchar12;
                            // l1c=code[effective(ptr++)];
                            // dict[ptr2]=l1c;
                        } while ((dictchar12 & 0x80) == 0);
                        areg[0].Write16(ptr12_a0);
                        areg[1].Write16(ptr12_a1);
                        break;

                    case 13: /* A0EA A1=write_dictword(A1,D1=output_mode) */
                        var ptr13_a1 = areg[1].Read16();
                        var tmp13 = dreg[3].Read8();
                        dreg[3].Write8(dreg[1].Read8());
                        byte dictchar;
                        do
                        {
                            dictchar = dict[ptr13_a1++];
                            byte_out(dictchar);
                        } while (dictchar < 0x80);
                        areg[1].Write16(ptr13_a1);
                        dreg[3].Write8(tmp13);
                        break;

                    case 14: /* A0EB [3037 - j] */
                        dict[areg[1].Read16()] = dreg[1].Read8();
                        break;

                    case 15: /* A0EC */
                        dreg[1].Write8(dict[areg[1].Read16()]);
                        break;

                    case 16:
                        ms_stop(); /* infinite loop A0ED */
                        break;
                    case 17:
                        if (ms_init(null, null, null) == 0)
                            ms_stop(); /* restart game ie. pc, sp etc. A0EE */
                        break;
                    case 18: // ms_fatal("undefined op 11 in do_line_a"); /*
                             // undefined A0EF */
                        break;
                    case 19:
                        // System.err.println("Showpic: "+ (byte)read_reg(0,0) );

                        ms_showpic(dreg[0].Read8(), null, dreg[1].Read8()); /*
																			 * Do_picture(D0)
																			 * A0F0
																			 */
                        break;
                    case 20:
                        var ptr20_a1 = areg[1].Read16(); /* A1=nth_string(A1,D0) A0F1 */
                        var tmp16_20 = dreg[0].Read16();
                        while (tmp16_20-- > 0)
                        {
                            while (code[effective(ptr20_a1++)] != 0) ;
                        }
                        areg[1].Write16(ptr20_a1);
                        break;

                    case 21: /* [2a43] A0F2 */
                        cflag = false;
                        dreg[0].Write16(dreg[2].Read16());
                        do_findprop();
                        var ptr21_a0 = (uint)areg[0].Read16();
                        while (dreg[2].Read16() > 0)
                        {
                            if ((code.Read16(effective(ptr21_a0 + 12)) & 0x3fff) != 0)
                            {
                                cflag = true;
                                break;
                            }
                            if (dreg[2].Read16() == (dreg[4].Read16() & 0x7fff))
                            {
                                cflag = true;
                                break;
                            }
                            ptr21_a0 -= 0x0e;
                            dreg[2].Write16((ushort)(dreg[2].Read16() - 1));
                        }
                        break;

                    case 22:
                        byte_out(dreg[1].Read8()); /* A0F3 */
                        break;

                    case 23: /* D7=Save_(filename A0) D1 bytes starting from A1 A0F4 */
                        byte[] savestr;
                        if (version < 4)
                        {
                            var str_offset = (version < 4) ? effective(areg[0].Read16()) : 0;
                            var str_len = findnull(code, str_offset);
                            if (str_len > 0)
                            {
                                savestr = new byte[str_len];
                                Array.Copy(code, str_offset, savestr, 0, str_len);
                            }
                            else
                            {
                                savestr = null;
                            }
                        }
                        else
                        {
                            savestr = null;
                        }
                        var chararray = savestr != null ? savestr.Select(p => (char)p).ToArray() : null;

                        var size = (int)dreg[1].Read16();
                        var saveoffset = (int)effective(areg[1].Read16());
                        var saveArray = new byte[size];
                        Array.Copy(this.code, saveoffset, saveArray, 0, size);
                        dreg[7].Write8(ms_save_file(chararray, saveArray));
                        break;

                    case 24: /* D7=Load_(filename A0) D1 bytes starting from A1 A0F5 */
                        byte[] loadstr;
                        if (version < 4)
                        {
                            var str_offset = (version < 4) ? effective(areg[0].Read16()) : 0;
                            var str_len = findnull(code, str_offset);
                            if (str_len > 0)
                            {
                                loadstr = new byte[str_len];
                                Array.Copy(code, str_offset, loadstr, 0, str_len);
                            }
                            else
                            {
                                loadstr = null;
                            }
                        }
                        else
                        {
                            loadstr = null;
                        }
                        var loadArray = loadstr != null ? loadstr.Select(p => (char)p).ToArray() : null;

                        var loadsize = (int)dreg[1].Read16();
                        var loadBuffer = new byte[loadsize];
                        var loadOffset = (int)effective(areg[1].Read16());
                        var loadResult = ms_load_file(loadArray, loadBuffer);
                        if (loadResult == 0)
                        {
                            Array.Copy(loadBuffer, 0, this.code, loadOffset, loadsize);
                        }
                        dreg[7].Write16(loadResult);
                        break;

                    case 25: /* D1=Random(0..D1-1) [3748] A0F6 */
                        var l1c25 = dreg[1].Read8();
                        dreg[1].Write16((ushort)(rand_emu() % ((l1c25 != 0) ? l1c25 : 1)));
                        break;

                    case 26: /* D0=Random(0..255) [3742] A0F7 */
                        var rand = rand_emu();
                        dreg[0].Write8((byte)(rand + (rand >> /*TS*/ 8)));
                        break;

                    case 27: /* write string [D0] [2999] A0F8 */
                        write_string();
                        break;

                    case 28: /* Z,D0=Get_inventory_item(D0) [2a9e] A0F9 */
                        zflag = false;
                        var tmp16_28 = dreg[0].Read16();
                        do
                        {
                            dreg[0].Write16(tmp16_28);
                            do
                            {
                                do_findprop();
                                var ptr28_a0 = (uint)areg[0].Read16(); /* object properties */
                                if (((code[effective(ptr28_a0) + 5]) & 1) != 0)
                                    break; /* is_described or so */
                                var l1c28 = code[effective(ptr28_a0) + 6]; /* some_flags */
                                var parent16 = code.Read16(effective(ptr28_a0 + 8)); /* parent_object */
                                if (l1c28 == 0)
                                { /* ordinary object? */
                                    if (parent16 == 0)
                                        zflag = true; /* return if parent()=player */
                                    break; /* otherwise try next */
                                }
                                if ((l1c28 & 0xcc) != 0)
                                {
                                    break;
                                } /* skip worn, bodypart, room, hidden */
                                if (parent16 == 0)
                                { /* return if parent()=player? */
                                    zflag = true;
                                    break;
                                }
                                dreg[0].Write16(parent16); /* else look at parent() */
                            } while (1 == 1);
                            tmp16_28--;
                        } while ((zflag == false) && (tmp16_28 != 0));
                        dreg[0].Write16((ushort)(tmp16_28 + 1));
                        break;

                    case 29: /* [2b18] A0FA */
                        var ptr29_a0 = areg[0].Read16();
                        bool bool29 = false;
                        do
                        {
                            if (dreg[5].Read8() != 0)
                            {
                                bool29 = ((code.Read16(effective(ptr29_a0)) & 0x3fff) == dreg[2].Read16());
                            }
                            else
                            {
                                bool29 = (code[effective(ptr29_a0)] == dreg[2].Read8());
                            }
                            if (dreg[3].Read16() == dreg[4].Read16())
                            {
                                cflag = false;
                                areg[0].Write16(ptr29_a0);
                            }
                            else
                            {
                                dreg[3].Write16((ushort)(dreg[3].Read16() + 1));
                                ptr29_a0 += 0x0e;
                                if (bool29)
                                {
                                    cflag = true;
                                    areg[0].Write16(ptr29_a0);
                                }
                            }
                        } while (bool29 == false && (dreg[3].Read16() != dreg[4].Read16()));
                        break;

                    case 30: /* [2bd1] A0FB */
                        var ptr30_a1 = areg[1].Read16();
                        do
                        {
                            if (dict != null)
                                while (dict[ptr30_a1++] < 0x80)
                                    ;
                            else
                                while (code[effective(ptr30_a1++)] < 0x80)
                                    ;
                            dreg[2].Write16((ushort)(dreg[2].Read16() - 1));
                        } while (dreg[2].Read16() != 0);
                        areg[1].Write16(ptr30_a1);
                        break;

                    case 31: /* [2c3b] A0FC */
                        var ptr31_a0 = areg[0].Read16();
                        var ptr31_a1 = areg[1].Read16();
                        do
                        {
                            if (dict != null)
                                while (dict[ptr31_a0++] < 0x80)
                                    ;
                            else
                                while (code[effective(ptr31_a0++)] < 0x80)
                                    ;
                            while (code[effective(ptr31_a1++)] != 0)
                                ;
                            dreg[0].Write16((ushort)(dreg[0].Read16() - 1));
                        } while (dreg[0].Read16() != 0);
                        areg[0].Write16(ptr31_a0);
                        areg[1].Write16(ptr31_a1);
                        break;

                    case 32: /* Set properties pointer from A0 [2b7b] A0FD */
                        properties = areg[0].Read16();
                        if (version > 0)
                            fl_sub = areg[3].Read16();
                        if (version > 1)
                        {
                            fl_tab = areg[5].Read16();
                            fl_size = (ushort)(dreg[7].Read16() + 1);
                            /* A3 [routine], A5 [table] and D7 [table-size] */
                        }
                        if (version > 2)
                        {
                            fp_tab = areg[6].Read16();
                            fp_size = dreg[6].Read16();
                        }
                        break;

                    case 33:
                        do_findprop(); /* A0FE */
                        break;

                    case 34: /* Dictionary_lookup A0FF */
                        dict_lookup();
                        break;
                }
            }
        }

        /* emulate an instruction [1b7e] */

        public bool ms_rungame()
        {
            if (running == false)
                return running;
            if (pc == undo_pc)
                save_undo();

            if (LOGEMU)
            {
                if (pc != 0x0000)
                {
                    //log_status();
                }

                //log("logemu", "" + pc);
            }

            i_count++;

            // if ((i_count >= 250000) && (i_count <= 280000))
            // {
            // //Console.WriteLine(i_count);
            // //ms_status();
            // }
            //	
            // if (i_count == 55311)
            // {
            // //Console.WriteLine("REACHED");
            // //ms_status();
            // }
            read_word();
            switch (byte1 >> /*TS*/ 1)
            {

                /* 00-0F */
                case 0x00:
                    if (byte1 == 0x00)
                    {
                        if (byte2 == 0x3c || byte2 == 0x7c)
                        {
                            /* OR immediate to CCR (30D9) */
                            read_word();
                            if (LOGEMU)
                            {
                                log("logemu", "or_ccr #" + byte2);
                            }
                            if ((byte2 & 0x01) != 0)
                                cflag = true;
                            if ((byte2 & 0x02) != 0)
                                vflag = true;
                            if ((byte2 & 0x04) != 0)
                                zflag = true;
                            if ((byte2 & 0x08) != 0)
                                nflag = true;
                        }
                        else
                        { /* OR [27df] */
                            if (LOGEMU)
                            {
                                log("logemu", "or");
                            }
                            get_arg();
                            do_or();
                        }
                    }
                    else
                        check_btst();
                    break;

                case 0x01:
                    if (byte1 == 0x02)
                    {
                        if (byte2 == 0x3c || byte2 == 0x7c)
                        {
                            /* AND immediate to CCR */
                            read_word();
                            if (LOGEMU)
                            {
                                log("logemu", "and_ccr #" + byte2);
                            }
                            if ((byte2 & 0x01) == 0)
                                cflag = false;
                            if ((byte2 & 0x02) == 0)
                                vflag = false;
                            if ((byte2 & 0x04) == 0)
                                zflag = false;
                            if ((byte2 & 0x08) == 0)
                                nflag = false;
                        }
                        else
                        { /* AND */
                            if (LOGEMU)
                            {
                                log("logemu", "and");
                            }
                            get_arg();
                            do_and();
                        }
                    }
                    else
                        check_btst();
                    break;

                case 0x02:
                    if (byte1 == 0x04)
                    { /* SUB */
                        if (LOGEMU)
                        {
                            log("logemu", "sub");
                        }
                        get_arg();
                        do_sub(false);
                    }
                    else
                        check_btst();
                    break;

                case 0x03:
                    if (byte1 == 0x06)
                    { /* ADD */
                        if (LOGEMU)
                        {
                            log("logemu", "addi");
                        }
                        get_arg();
                        do_add(false);
                    }
                    else
                        check_btst();
                    break;

                case 0x04:
                    if (byte1 == 0x08)
                    { /* bit operations (immediate) */
                        set_info((byte)(byte2 & 0x3f));
                        var l1c04 = code[effective(pc) + 1];
                        pc += 2;
                        set_arg1();
                        do_bop(byte2, l1c04);
                    }
                    else
                        check_btst();
                    break;

                case 0x05:
                    if (byte1 == 0x0a)
                    {
                        if (byte2 == 0x3c || byte2 == 0x7c)
                        {
                            /* EOR immediate to CCR */
                            read_word();
                            if (LOGEMU)
                            {
                                log("logemu", "eor_ccr #" + byte2);
                            }
                            if ((byte2 & 0x01) != 0)
                                cflag ^= true;
                            if ((byte2 & 0x02) != 0)
                                vflag ^= true;
                            if ((byte2 & 0x04) != 0)
                                zflag ^= true;
                            if ((byte2 & 0x08) != 0)
                                nflag ^= true;
                        }
                        else
                        { /* EOR */
                            if (LOGEMU)
                            {
                                log("logemu", "eor");
                            }
                            get_arg();
                            do_eor();
                        }
                    }
                    else
                        check_btst();
                    break;

                case 0x06:
                    if (byte1 == 0x0c)
                    { /* CMP */
                        if (LOGEMU)
                        {
                            log("logemu", "cmp");
                        }
                        get_arg();
                        do_cmp();
                    }
                    else
                        check_btst();
                    break;

                case 0x07:
                    check_btst();
                    break;

                /* 10-1F [3327] MOVE.B */
                case 0x08:
                case 0x09:
                case 0x0a:
                case 0x0b:
                case 0x0c:
                case 0x0d:
                case 0x0e:
                case 0x0f:

                    if (LOGEMU)
                    {
                        log("logemu", "move.b");
                    }
                    set_info((byte)(byte2 & 0x3f));
                    set_arg1();
                    swap_args();
                    var l1c08 = (byte)(((byte1 >> /*TS*/ 1 & 0x07) | (byte2 >> /*TS*/ 3 & 0x18) | (byte1 << 5 & 0x20)));
                    set_info(l1c08);
                    set_arg1();
                    do_move();
                    break;

                /* 20-2F [32d1] MOVE.L */
                case 0x10:
                case 0x11:
                case 0x12:
                case 0x13:
                case 0x14:
                case 0x15:
                case 0x16:
                case 0x17:
                    if (LOGEMU)
                    {
                        log("logemu", "move.l");
                    }

                    set_info((byte)(byte2 & 0x3f | 0x80));
                    set_arg1();
                    swap_args();
                    var l1c10= (byte)(((byte1 >> /*TS*/ 1 & 0x07) | (byte2 >> /*TS*/ 3 & 0x18) | (byte1 << 5 & 0x20)));
                    set_info((byte)(l1c10 | 0x80));
                    set_arg1();
                    do_move();
                    break;

                /* 30-3F [3327] MOVE.W */
                case 0x18:
                case 0x19:
                case 0x1a:
                case 0x1b:
                case 0x1c:
                case 0x1d:
                case 0x1e:
                case 0x1f:

                    if (LOGEMU)
                    {
                        log("logemu", "move.w");
                    }
                    set_info((byte)(byte2 & 0x3f | 0x40));
                    set_arg1();
                    swap_args();
                    var l1c18 = (byte)((byte1 >> /*TS*/ 1 & 0x07) | (byte2 >> /*TS*/ 3 & 0x18) | (byte1 << 5 & 0x20));
                    set_info((byte)(l1c18 | 0x40));
                    set_arg1();
                    do_move();
                    break;

                /* 40-4F various commands */

                case 0x20:
                    if (byte1 == 0x40)
                    { /* [31d5] */
                        ms_fatal("unimplemented instructions NEGX and MOVE SR,xx");
                    }
                    else
                        check_lea();
                    break;

                case 0x21:
                    if (byte1 == 0x42)
                    { /* [3188] */
                        if ((byte2 & 0xc0) == 0xc0)
                        {
                            ms_fatal("unimplemented instruction MOVE CCR,xx");
                        }
                        else
                        { /* CLR */
                            if (LOGEMU)
                            {
                                log("logemu", "clr");
                            }
                            set_info(byte2);
                            set_arg1();
                            if (opsize == 0)
                                acc1.Write8(arg1, 0);
                            if (opsize == 1)
                                acc1.Write16(arg1, 0);
                            if (opsize == 2)
                                acc1.Write32(arg1, 0);
                            nflag = cflag = false;
                            zflag = true;
                        }
                    }
                    else
                        check_lea();
                    break;

                case 0x22:
                    if (byte1 == 0x44)
                    { /* [31a0] */
                        if ((byte2 & 0xc0) == 0xc0)
                        { /* MOVE to CCR */
                            if (LOGEMU)
                            {
                                log("logemu", "move_ccr");
                            }
                            zflag = nflag = cflag = vflag = false;
                            set_info((byte)(byte2 & 0x7f));
                            set_arg1();
                            byte2 = acc1.Read8(arg1 + 1);
                            cflag = (byte2 & 0x01) != 0;
                            vflag = (byte2 & 0x02) != 0;
                            zflag = (byte2 & 0x04) != 0;
                            nflag = (byte2 & 0x08) != 0;
                        }
                        else
                        {
                            if (LOGEMU)
                            {
                                log("logemu", "neg");
                            }
                            set_info(byte2); /* NEG */
                            set_arg1();
                            cflag = true;
                            if (opsize == 0)
                            {
                                acc1.Write8(arg1, (byte)(-1 * acc1.Read8(arg1) + 256));
                                cflag = ((acc1.Read8(arg1)) != 0);
                            }
                            if (opsize == 1)
                            {
                                acc1.Write16(arg1, (ushort)(-1 * acc1.Read16(arg1)));
                                cflag = ((acc1.Read16(arg1)) != 0);
                            }
                            if (opsize == 2)
                            {
                                acc1.Write32(arg1, (uint)(-1 * acc1.Read32(arg1)));
                                cflag = (acc1.Read32(arg1) != 0);
                            }
                            vflag = false;
                            set_flags();
                        }
                    }
                    else
                        check_lea();
                    break;

                case 0x23:
                    if (byte1 == 0x46)
                    {
                        if ((byte2 & 0xc0) == 0xc0)
                        {
                            ms_fatal("unimplemented instruction MOVE xx,SR");
                        }
                        else
                        {
                            if (LOGEMU)
                            {
                                log("logemu", "not");
                            }
                            set_info(byte2); /* NOT */
                            set_arg1();
                            acc2 = new byte[] { 0xFF, 0xFF, 0xFF, 0xFF } ;
                            arg2 = 0;
                            do_eor();
                        }
                    }
                    else
                        check_lea();
                    break;

                case 0x24:
                    if (byte1 == 0x48)
                    {
                        if ((byte2 & 0xf8) == 0x40)
                        {
                            if (LOGEMU)
                            {
                                log("logemu", "swap");
                            }
                            opsize = 2; /* SWAP */
                            admode = AddressMode.VM68K_AM_DATAREG;
                            regnr = (byte)(byte2 & 0x07);
                            set_arg1();
                            var tmp32 = acc1.Read16(arg1);
                            acc1.Write16(arg1, acc1.Read16(arg1 + 2));
                            acc1.Write16(arg1 + 2, tmp32); /*
													 * PROBLEMATIC with long
													 * bounds??
													 */
                            set_flags();
                        }
                        else if ((byte2 & 0xf8) == 0x80)
                        {
                            if (LOGEMU)
                            {
                                log("logemu", "ext.w");
                            }
                            opsize = 1; /* EXT.W */
                            admode = AddressMode.VM68K_AM_DATAREG;
                            regnr = (byte)(byte2 & 0x07);
                            set_arg1();
                            if (acc1.Read8(arg1 + 1) > 0x7f)
                                acc1.Write8(arg1, 0xff);
                            else
                                acc1.Write8(arg1, 0);
                            set_flags();
                        }
                        else if ((byte2 & 0xf8) == 0xc0)
                        {
                            if (LOGEMU)
                            {
                                log("logemu", "ext.l");
                            }
                            opsize = 2; /* EXT.L */
                            admode = AddressMode.VM68K_AM_DATAREG;
                            regnr = (byte)(byte2 & 0x07);
                            set_arg1();
                            if (acc1.Read16(arg1 + 2) > 0x7fff)
                                acc1.Write16(arg1, 0xffff);
                            else
                                acc1.Write16(arg1, 0);
                            set_flags();
                        }
                        else if ((byte2 & 0xc0) == 0x40)
                        {
                            if (LOGEMU)
                            {
                                log("logemu", "pea");
                            }
                            set_info((byte)(byte2 & 0x3f | 0x80)); /* PEA */
                            var arg = set_arg1();
                            if (arg.is_reversible)
                                push(arg.arg1i);
                            else
                                ms_fatal("illegal addressing mode for PEA");
                        }
                        else
                        {
                            check_movem(); /* MOVEM */
                        }
                    }
                    else
                        check_lea();
                    break;

                case 0x25:
                    if (byte1 == 0x4a)
                    { /* [3219] TST */
                        if ((byte2 & 0xc0) == 0xc0)
                        {
                            ms_fatal("unimplemented instruction TAS");
                        }
                        else
                        {
                            if (LOGEMU)
                            {
                                log("logemu", "tst");
                            }
                            set_info(byte2);
                            set_arg1();
                            cflag = vflag = false;
                            set_flags();
                        }
                    }
                    else
                        check_lea();
                    break;

                case 0x26:
                    if (byte1 == 0x4c)
                        check_movem2(); /* [3350] MOVEM.L (Ax)+,A/Dx */
                    else
                        check_lea(); /* LEA */
                    break;

                case 0x27:
                    if (byte1 == 0x4e)
                    { /* [3290] */
                        if (byte2 == 0x75)
                        { /* RTS */
                            if (LOGEMU)
                            {
                                log("logemu", "rts\n");
                            }
                            pc = pop();
                        }
                        else if (byte2 == 0x71)
                        { /* NOP */
                            if (LOGEMU)
                            {
                                log("logemu", "nop");
                            }
                        }
                        else if ((byte2 & 0xc0) == 0xc0)
                        { /* indir JMP */
                            if (LOGEMU)
                            {
                                log("logemu", "jmp");
                            }
                            set_info((byte)(byte2 | 0xc0));
                            var arg = set_arg1();
                            if (arg.is_reversible)
                                pc = arg.arg1i;
                            else
                                ms_fatal("illegal addressing mode for JMP");
                        }
                        else if ((byte2 & 0xc0) == 0x80)
                        {
                            if (LOGEMU)
                            {
                                log("logemu", "jsr");
                            }
                            set_info((byte)(byte2 | 0xc0)); /* indir JSR */
                            var arg = set_arg1();
                            push(pc);
                            if (arg.is_reversible)
                                pc = arg.arg1i;
                            else
                                ms_fatal("illegal addressing mode for JSR");
                        }
                        else
                        {
                            ms_fatal("unimplemented instructions 0x4EXX");
                        }
                    }
                    else
                        check_lea(); /* LEA */
                    break;

                /* 50-5F [2ed5] ADDQ/SUBQ/Scc/DBcc */
                case 0x28:
                case 0x29:
                case 0x2a:
                case 0x2b:
                case 0x2c:
                case 0x2d:
                case 0x2e:
                case 0x2f:

                    if ((byte2 & 0xc0) == 0xc0)
                    {
                        set_info((byte)(byte2 & 0x3f));
                        set_arg1();
                        if (admode == AddressMode.VM68K_AM_ADDRREG)
                        { /* DBcc */
                            if (LOGEMU)
                            {
                                log("logemu", "dbcc");
                            }
                            if (condition(byte1) == false)
                            {
                                // arg1=(arg1-(type8 *)areg)+(type8 *)dreg-1; /* nasty
                                // */
                                // really nasty, hope this works...
                                if (arg1 < 1)
                                    ms_fatal(" DBcc bounds violation - ms_rungame 0x28");
                                setargsrc(1, dreg[regnr].bytes, arg1 - 1);
                                acc1.Write16(arg1, (ushort)(acc1.Read16(arg1) - 1));
                                if (acc1.Read16(arg1) != 0xffff)
                                {
                                    branch(0);
                                }
                                else
                                {
                                    pc += 2;
                                }
                            }
                            else
                            {
                                pc += 2;
                            }
                        }
                        else
                        { /* Scc */
                            if (LOGEMU)
                            {
                                log("logemu", "scc");
                            }
                            acc1.Write8(arg1, (condition(byte1) ? (byte)0xff : (byte)0));
                        }
                    }
                    else
                    {
                        set_info(byte2);
                        set_arg1();
                        quick_flag = (admode == AddressMode.VM68K_AM_ADDRREG);
                        var l1c28 = (byte)(byte1 >> /*TS*/ 1 & 0x07);
                        var tmparg = new byte[] {0,0,0, (l1c28 != 0) ? l1c28 : (byte)8 };
                        setargsrc(2, tmparg, reg_align(opsize));
                        long outnum = 0;
                        if (LOGEMU)
                        {
                            switch (opsize)
                            {
                                case 0:
                                    outnum = acc1[arg2];
                                    break;
                                case 1:
                                    outnum = (short)acc1.Read16(arg2);
                                    break;
                                case 2:
                                    outnum = (int)acc1.Read32(arg2);
                                    break;
                            }
                        }
                        if ((byte1 & 0x01) == 1)
                        {
                            if (LOGEMU)
                            {
                                log("logemu", "subq #" + outnum);
                            }
                            do_sub(false);
                        } /* SUBQ */
                        else
                        {
                            if (LOGEMU)
                            {
                                log("logemu", "addq #" + outnum);
                            }
                            do_add(false);
                        } /* ADDQ */
                    }
                    break;

                /* 60-6F [26ba] Bcc */

                case 0x30:
                    if (byte1 == 0x61)
                    { /* BRA, BSR */
                        if (LOGEMU)
                        {
                            log("logemu", "bsr");
                        }
                        if (byte2 == 0)
                            push(pc + 2);
                        else
                            push(pc);
                    }
                    else
                    {
                        if (LOGEMU)
                        {
                            log("logemu", "bra");
                        }
                    }
                    if ((byte1 == 0x60) && (byte2 == 0xfe))
                    {
                        ms_flush(); /* flush stdout */
                        ms_stop(); /* infinite loop - just exit */
                    }
                    branch(byte2);
                    break;

                case 0x31:
                case 0x32:
                case 0x33:
                case 0x34:
                case 0x35:
                case 0x36:
                case 0x37:

                    if (condition(byte1) == false)
                    {
                        if (LOGEMU)
                        {
                            log("logemu", "beq.s");
                        }
                        if (byte2 == 0)
                            pc += 2;
                    }
                    else
                    {
                        if (LOGEMU)
                        {
                            log("logemu", "bra");
                        }
                        branch(byte2);
                    }
                    break;

                /* 70-7F [260a] MOVEQ */
                case 0x38:
                case 0x39:
                case 0x3a:
                case 0x3b:
                case 0x3c:
                case 0x3d:
                case 0x3e:
                case 0x3f:

                    if (LOGEMU)
                    {
                        log("logemu", "move.q");
                    }
                    setargsrc(1, dreg[byte1 >> /*TS*/ 1 & 0x07].bytes, 0);
                    if (byte2 > 127)
                    {
                        acc1.Write8(arg1, 0xff);
                        acc1.Write8(arg1 + 1, 0xff);
                        acc1.Write8(arg1 + 2, 0xff);
                        nflag = true;

                    }
                    else
                    {
                        nflag = false;
                        acc1.Write8(arg1, 0);
                        acc1.Write8(arg1 + 1, 0);
                        acc1.Write8(arg1 + 2, 0);
                    }
                    acc1.Write8(arg1 + 3, byte2);
                    zflag = (byte2 == 0);
                    break;

                /* 80-8F [2f36] */
                case 0x40:
                case 0x41:
                case 0x42:
                case 0x43:
                case 0x44:
                case 0x45:
                case 0x46:
                case 0x47:

                    if ((byte2 & 0xc0) == 0xc0)
                    {
                        ms_fatal("unimplemented instructions DIVS and DIVU");
                    }
                    else if (((byte2 & 0xf0) == 0) && ((byte1 & 0x01) != 0))
                    {
                        ms_fatal("unimplemented instruction SBCD");
                    }
                    else
                    {
                        if (LOGEMU)
                        {
                            log("logemu", "or?");
                        }
                        set_info(byte2);
                        set_arg1();
                        set_arg2(true, byte1);
                        if ((byte1 & 0x01) == 0)
                            swap_args();
                        do_or();
                    }
                    break;

                /* 90-9F [3005] SUB */
                case 0x48:
                case 0x49:
                case 0x4a:
                case 0x4b:
                case 0x4c:
                case 0x4d:
                case 0x4e:
                case 0x4f:

                    if (LOGEMU)
                    {
                        log("logemu", "sub");
                    }
                    quick_flag = false;
                    if ((byte2 & 0xc0) == 0xc0)
                    {
                        if ((byte1 & 0x01) == 1)
                            set_info((byte)(byte2 & 0xbf));
                        else
                            set_info((byte)(byte2 & 0x7f));
                        set_arg1();
                        set_arg2_nosize(false, byte1);
                        // set_arg2(false,byte1);
                        swap_args();
                        do_sub(true);
                    }
                    else
                    {
                        set_info(byte2);
                        set_arg1();
                        set_arg2(true, byte1);
                        if ((byte1 & 0x01) == 0)
                            swap_args();
                        do_sub(false);

                    }
                    break;

                /* A0-AF various special commands [LINE_A] */

                case 0x50:
                case 0x56: /* [2521] */
                case 0x57:
                    do_line_a();
                    if (LOGEMU)
                    {
                        log("logemu", "LINE_A A0" + byte2);
                    }
                    break;

                case 0x51:
                    if (LOGEMU)
                    {
                        log("logemu", "rts\n");
                    }
                    pc = pop(); /* RTS */
                    break;

                case 0x52:
                    if (LOGEMU)
                    {
                        log("logemu", "bsr");
                    }

                    if (byte2 == 0)
                        push(pc + 2); /* BSR */
                    else
                        push(pc);
                    branch(byte2);
                    break;

                case 0x53:
                    if ((byte2 & 0xc0) == 0xc0)
                    { /* TST [321d] */
                        ms_fatal("unimplemented instructions LINE_A #$6C0-#$6FF");
                    }
                    else
                    {
                        if (LOGEMU)
                        {
                            log("logemu", "tst");
                        }
                        set_info(byte2);
                        set_arg1();
                        cflag = vflag = false;
                        set_flags();
                    }
                    break;

                case 0x54:
                    check_movem();
                    break;

                case 0x55:
                    check_movem2();
                    break;

                /* B0-BF [2fe4] */
                case 0x58:
                case 0x59:
                case 0x5a:
                case 0x5b:
                case 0x5c:
                case 0x5d:
                case 0x5e:
                case 0x5f:

                    if ((byte2 & 0xc0) == 0xc0)
                    {
                        if (LOGEMU)
                        {
                            log("logemu", "cmp");
                        }
                        if ((byte1 & 0x01) == 1)
                            set_info((byte)(byte2 & 0xbf));
                        else
                            set_info((byte)(byte2 & 0x7f));
                        set_arg1();
                        set_arg2(false, byte1);
                        swap_args();
                        do_cmp(); /* CMP */
                    }
                    else
                    {
                        if ((byte1 & 0x01) == 0)
                        {
                            if (LOGEMU)
                            {
                                log("logemu", "cmp");
                            }
                            set_info(byte2);
                            set_arg1();
                            set_arg2(true, byte1);
                            swap_args();
                            do_cmp(); /* CMP */
                        }
                        else
                        {
                            if (LOGEMU)
                            {
                                log("logemu", "eor");
                            }
                            set_info(byte2);
                            set_arg1();
                            set_arg2(true, byte1);
                            do_eor(); /* EOR */
                        }
                    }
                    break;

                /* C0-CF [2f52] */
                case 0x60:
                case 0x61:
                case 0x62:
                case 0x63:
                case 0x64:
                case 0x65:
                case 0x66:
                case 0x67:

                    if ((byte1 & 0x01) == 0)
                    {
                        if ((byte2 & 0xc0) == 0xc0)
                        {
                            ms_fatal("unimplemented instruction MULU");
                        }
                        else
                        { /* AND */
                            if (LOGEMU)
                            {
                                log("logemu", "and");
                            }
                            set_info(byte2);
                            set_arg1();
                            set_arg2(true, byte1);
                            if ((byte1 & 0x01) == 0)
                                swap_args();
                            do_and();
                        }
                    }
                    else
                    {
                        if ((byte2 & 0xf8) == 0x40)
                        {
                            if (LOGEMU)
                            {
                                log("logemu", "exg (dx)");
                            }
                            opsize = 2; /* EXG Dx,Dx */
                            set_arg2(true, (byte)(byte2 << 1));
                            swap_args();
                            set_arg2(true, byte1);
                            var tmp32 = acc1.Read32(arg1);
                            acc1.Write32(arg1, acc2.Read32(arg2));
                            acc2.Write32(arg2, tmp32);
                        }
                        else if ((byte2 & 0xf8) == 0x48)
                        {
                            if (LOGEMU)
                            {
                                log("logemu", "exg (ax)");
                            }
                            opsize = 2; /* EXG Ax,Ax */
                            set_arg2(false, (byte)(byte2 << 1));
                            swap_args();
                            set_arg2(false, byte1);
                            var tmp32 = acc1.Read32(arg1);
                            acc1.Write32(arg1, acc2.Read32(arg2));
                            acc2.Write32(arg2, tmp32);
                        }
                        else if ((byte2 & 0xf8) == 0x88)
                        {
                            if (LOGEMU)
                            {
                                log("logemu", "exg (dx,ax)");
                            }
                            opsize = 2; /* EXG Dx,Ax */
                            set_arg2(false, (byte)(byte2 << 1));
                            swap_args();
                            set_arg2(true, byte1);
                            var tmp32 = acc1.Read32(arg1);
                            acc1.Write32(arg1, acc2.Read32(arg2));
                            acc2.Write32(arg2, tmp32);
                        }
                        else
                        {
                            if ((byte2 & 0xc0) == 0xc0)
                            {
                                ms_fatal("unimplemented instruction MULS");
                            }
                            else
                            {
                                set_info(byte2);
                                set_arg1();
                                set_arg2(true, byte1);
                                if ((byte1 & 0x01) == 0)
                                    swap_args();
                                do_and();
                            }
                        }
                    }
                    break;

                /* D0-DF [2fc8] ADD */
                case 0x68:
                case 0x69:
                case 0x6a:
                case 0x6b:
                case 0x6c:
                case 0x6d:
                case 0x6e:
                case 0x6f:

                    if (LOGEMU)
                    {
                        log("logemu", "add");
                    }
                    quick_flag = false;
                    if ((byte2 & 0xc0) == 0xc0)
                    {
                        if ((byte1 & 0x01) == 1)
                            set_info((byte)(byte2 & 0xbf));
                        else
                            set_info((byte)(byte2 & 0x7f));
                        set_arg1();
                        set_arg2_nosize(false, byte1);
                        // set_arg2(false,byte1);
                        swap_args();
                        do_add(true);
                    }
                    else
                    {
                        set_info(byte2);
                        set_arg1();
                        set_arg2(true, byte1);
                        if ((byte1 & 0x01) == 0)
                            swap_args();
                        do_add(false);
                    }
                    break;

                /* E0-EF [3479] LSR ASL ROR ROL */
                case 0x70:
                case 0x71:
                case 0x72:
                case 0x73:
                case 0x74:
                case 0x75:
                case 0x76:
                case 0x77:
                    byte l1c;
                    if (LOGEMU)
                    {
                        log("logemu", "lsr,asl,ror,rol");
                    }
                    if ((byte2 & 0xc0) == 0xc0)
                    {
                        set_info((byte)(byte2 & 0xbf)); /* OP Dx */
                        set_arg1();
                        l1c = 1; /* steps=1 */
                        byte2 = (byte)((byte1 >> /*TS*/ 1) & 0x03);
                    }
                    else
                    {
                        set_info((byte)(byte2 & 0xc7));
                        set_arg1();
                        if ((byte2 & 0x20) == 0)
                        { /* immediate */
                            l1c = (byte)((byte1 >> /*TS*/ 1) & 0x07);
                            if (l1c == 0)
                                l1c = 8;
                        }
                        else
                        {
                            l1c = dreg[byte1 >> /*TS*/ 1 & 0x07].Read8();
                        }
                        byte2 = (byte)((byte2 >> 3) & 0x03);
                    }
                    if ((byte1 & 0x01) == 0)
                    { /* right */
                        while (l1c-- > 0)
                        {
                            if (opsize == 0)
                            {
                                cflag = ((acc1.Read8(arg1) & 0x01) != 0);
                                acc1.Write8(arg1, (byte)(acc1.Read8(arg1) >> /*TS*/ 1));
                                if ((cflag) && (byte2 == 3))
                                    acc1.Write8(arg1,
                                            (byte)(acc1.Read8(arg1) | 0x80));
                            }
                            if (opsize == 1)
                            {
                                cflag = ((acc1.Read16(arg1) & 0x01) != 0);
                                acc1.Write16(arg1, (ushort)(acc1.Read16(arg1) >> /*TS*/ 1));
                                if ((cflag) && (byte2 == 3))
                                    acc1.Write16(arg1, (ushort)(acc1.Read16(arg1) | (1 << 15)));
                            }
                            if (opsize == 2)
                            {
                                cflag = ((acc1.Read32(arg1) & 0x01) != 0);
                                acc1.Write32(arg1, acc1.Read32(arg1) >> /*TS*/ 1);
                                if ((cflag) && (byte2 == 3))
                                    acc1.Write32(arg1, acc1.Read32(arg1) | ((uint)1 << 31));
                            }
                        }
                    }
                    else
                    { /* left */
                        while (l1c-- > 0)
                        {
                            if (opsize == 0)
                            {
                                cflag = ((acc1.Read8(arg1) & 0x80) != 0); /* [3527] */
                                acc1.Write8(arg1, (byte)(acc1.Read8(arg1) << 1));
                                if ((cflag) && (byte2 == 3))
                                    acc1.Write8(arg1, (byte)(acc1.Read8(arg1) | 0x01));
                            }
                            if (opsize == 1)
                            {
                                cflag = ((acc1.Read16(arg1) & (1 << 15)) != 0);
                                acc1.Write16(arg1, (ushort)(acc1.Read16(arg1) << 1));
                                if ((cflag) && (byte2 == 3))
                                    acc1.Write16(arg1, (ushort)(acc1.Read16(arg1) | 0x01));
                            }
                            if (opsize == 2)
                            {
                                cflag = ((acc1.Read32(arg1) & ((int)1 << 31)) != 0);
                                acc1.Write32(arg1, acc1.Read32(arg1) << 1);
                                if ((cflag) && (byte2 == 3))
                                    acc1.Write32(arg1, acc1.Read32(arg1) | 0x01);
                            }
                        }
                    }
                    set_flags();
                    break;

                /* F0-FF [24f3] LINE_F */
                case 0x78:
                case 0x79:
                case 0x7a:
                case 0x7b:
                case 0x7c:
                case 0x7d:
                case 0x7e:
                case 0x7f:

                    if (version == 0)
                    { /* hardcoded jump */
                        byte_out(dreg[1].Read8());
                    }
                    else if (version == 1)
                    { /* single programmable shortcut */
                        push(pc);
                        pc = fl_sub;
                    }
                    else
                    { /* programmable shortcuts from table */
                        if (LOGEMU)
                        {
                            log("logemu", "LINK: " + byte1 + "," + byte2);
                        }
                        var ptr = (ushort)((byte1 & 7) << 8 | byte2);
                        if (ptr >= fl_size)
                        {
                            if ((byte1 & 8) == 0)
                                push(pc);
                            ptr = (ushort)(byte1 << 8 | byte2 | 0x0800);
                            ptr = (ushort)(fl_tab + 2 * (ptr ^ 0xffff));
                            pc = (uint)((uint)(ptr) + (short)code.Read16(effective(ptr)));
                        }
                        else
                        {
                            push(pc);
                            pc = fl_sub;
                        }
                    }
                    break;

                default:
                    ms_fatal("Constants aren't and variables don't");
                    break;
            }
            return running;
        }

        void setargsrc(int argid, byte[] newdes, uint newcnt)
        {
            if (argid == 1)
            {
                acc1 = newdes;
                arg1 = newcnt;
            }
            else
            {
                acc2 = newdes;
                arg2 = newcnt;
            }
        }

        /* Find a terminating \0 in a byte array */
        public uint findnull(byte[] data, uint offset)
        {
            uint iLen = 0;
            while (data[offset + iLen] != 0)
                iLen++;
            return iLen;
        }

        public void run()
        {
            bool running = true;
            while (running)
            {
                running = ms_rungame();
            }
            ms_fatal("Exiting.");
        }

        public abstract byte ms_load_file(char[] name, byte[] loadBuffer);

        public abstract byte ms_save_file(char[] name, byte[] saveArray);

        public abstract void ms_showpic(int c, string picname, byte mode);

        public abstract void ms_fatal(string txt);

        public abstract void ms_playmusic(JMagneticSound name);


    }


}
