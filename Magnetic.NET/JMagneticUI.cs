﻿using Magnetic.NET.Graphics;
using Magnetic.NET.Sound;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnetic.NET
{
    interface JMagneticUI
    {
        /****************************************************************************\
        * Function: ms_load_file
        *
        * Purpose: Load a save game file and restore game
        *
        * Parameters:   char	name            string of filename typed by player
        *               int		ptr             offset in game memory where data start
        *               int		size            number of bytes to load
        *
        * Result: 0 is successful
        *
        * Note: You probably want to put in a file requester!
        \****************************************************************************/
        byte ms_load_file(char[] name, byte[] loadBuffer);

        /****************************************************************************\
        * Function: ms_save_file
        *
        * Purpose: Save the current game to file
        *
        * Parameters:   char	name            string of filename typed by player
        *               int		ptr             offset in game memory where data start
        *               int		size            number of bytes to save
        *
        * Result: 0 is successful
        *
        * Note: You probably want to put in a file requester!
        \****************************************************************************/
        byte ms_save_file(char[] name, byte[] saveArray);

        void ms_showpic(int c, string picName, byte mode);

        /* NEW */
        void ms_fatal(String txt);

        //int ms_showhints(IList<JMagneticHint> hints);
        bool ms_is_running();
        bool ms_is_magwin();
        void ms_stop();
        void ms_status();
        long ms_count();

        /* Really new */
        void ms_playmusic(JMagneticSound name);
    }




}
