﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnetic.NET
{
    public class Logger : ILogger
    {
        private static Dictionary<string, Logger> loggers = new Dictionary<string, Logger>();

        private StreamWriter file = null;

        public Logger(string name)
        {
            file = new System.IO.StreamWriter(name);
        }

        ~Logger()
        {
            //file.Close();
        }

        internal static Logger getRootLogger()
        {
            return getLogger("root");
        }

        internal static Logger getLogger(string p)
        {
            if (loggers.ContainsKey(p))
            {
                return loggers[p];
            }
            else
            {
                var logger = new Logger(p);
                loggers.Add(p, logger);
                return logger;
            }
        }

        public void debug(string p)
        {
            file.WriteLine("DEBUG:\t" + p);
        }

        public void info(string s)
        {
            file.WriteLine("INFO:\t" + s);
        }

        public void warn(string s)
        {
            file.WriteLine("WARN:\t" + s);
        }
    }
}
