﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnetic.NET
{
    public class NullLogger : ILogger
    {
        public void debug(string p)
        {
        }

        public void info(string s)
        {
        }

        public void warn(string s)
        {
        }
    }
}
