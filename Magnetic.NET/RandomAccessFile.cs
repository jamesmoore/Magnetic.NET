﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnetic.NET
{
    class RandomAccessFile
    {
        private byte[] byteArray;
        private int readPosition;

        public RandomAccessFile(string name, string mode)
        {
            this.byteArray = File.ReadAllBytes(name);
        }

        public void close()
        {
        }

        public void seek(long position)
        {
            this.readPosition = (int)position;
        }

        internal int read()
        {
            return byteArray[readPosition++];
        }

        internal int getFilePointer()
        {
            return readPosition;
        }

        private int readUnsignedByte()
        {
            return byteArray[readPosition++];
        }

        /* aux function to read unsigned bytes from file into byte array */
        public int ReadUbyteFromFile(byte[] des)
        {
            int iCnt, tmp;

            for (iCnt = 0; iCnt < des.Length; iCnt++)
            {
                tmp = this.readUnsignedByte();
                des[iCnt] = (byte)tmp;
            }
            return iCnt;
        }

        /* aux function to read count unsigned bytes from file into byte array */
        public int ReadUbyteFromFile(byte[] des, int Count)
        {
            int iCnt, tmp;

            for (iCnt = 0; iCnt < Count; iCnt++)
            {
                tmp = this.readUnsignedByte();
                des[iCnt] = (byte)tmp;
            }
            return iCnt;
        }

        public int ReadUbyteFromFile(byte[] des, uint Count)
        {
            return this.ReadUbyteFromFile(des, (int)Count);
        }
    }
}
