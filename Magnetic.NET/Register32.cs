﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnetic.NET
{
    internal class Register32
    {
        public Register32(ILogger logger, string name)
        {
            this.cat = logger;
            this.name = name;
        }

        private ILogger cat = null;
        private string name = null;
        public byte[] bytes = new byte[4];

        /**
         * Reset the register to 0L
         */
        public void clearReg()
        {
            cat.debug("Clear" + name);
            for (int i = 0; i < 4; i++) bytes[i] = 0;
        }

        /**
         * Read long from register
         *  
         * @return the long value from the register
        */
        public uint Read32()
        {
            return bytes.Read32(0);
        }

        /**
         * Read word from register

         * @param alignment at which offset of the register the read starts
         * @return the word from the register
         */
        public ushort Read16()
        {
            return bytes.Read16(2);
        }

        /**
         * Read byte from register

         * @param alignment at which offset of the register the read starts
         * @return the byte value from the register
         */
        public byte Read8()
        {
            return bytes.Read8(3);
        }

        /**
         * Write long value to register
         *
         * @param val Long value to be written to the register
         */
        public void Write32(uint val)
        {
            bytes.Write32(0, val);
            ////cat.debug("Write\t" + name + " " + val.ToString("X8"));
            //bytes[3] = ((byte)val);
            //val >>= 8;
            //bytes[2] = ((byte)val);
            //val >>= 8;
            //bytes[1] = ((byte)val);
            //val >>= 8;
            //bytes[0] = ((byte)val);
        }

        /**
         * Write word value to register starting at given offset
         *
         * @param alignment at which offset of the register the write starts
         * @param val Word to be written to the register
         */
        public void Write16(ushort val)
        {
            bytes.Write16(2, val);
            ////cat.debug("Write\t" + name + " " + val.ToString("X4"));
            //bytes[3] = ((byte)val);
            //val >>= 8;
            //bytes[2] = ((byte)val);
        }

        /**
         * Write byte value to register starting at given offset
         *
         * @param alignment at which offset of the register the write starts
         * @param val Byte value to be written to the register
         */
        public void Write8(byte val)
        {
            //cat.debug("Write\t" + name + " " + val.ToString("X2"));
            bytes[3] = val;
        }

    }

}
