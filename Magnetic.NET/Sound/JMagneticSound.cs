﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnetic.NET.Sound
{
    public class JMagneticSound
    {
        public String name;
        public uint tempo;
        public byte[] data;
    }
}
