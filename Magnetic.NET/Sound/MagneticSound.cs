﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnetic.NET.Sound
{
    public class MagneticSound
    {

        private bool LOGEMU = false;
        private bool LOGGFX = false;
        private bool LOGGFX_EXT = false;
        private bool LOGHNT = false;
        private bool LOGSND = false;

        private ILogger sndLogger;

        /* Sound support */
        protected IDictionary<string, JMagneticSound> sounds = new Dictionary<string, JMagneticSound>();

        public void init_snd(string sndname)
        {
            sounds.Clear();
            byte[] header3 = new byte[4];
            RandomAccessFile snd_fp;
            uint s_offset, s_length;
            snd_fp = new RandomAccessFile(sndname, "r");
            uint i;
            if (snd_fp == null)
            {
                // Console.WriteLine("Unable to open sound file
                // "+sndname);
                if (LOGEMU)
                    log("logemu", "Unable to open sound file "
                            + sndname);
            }
            else
            {
                // if ((ReadUbyteFromFile(fp,header) != 42) ||
                // (read_l(header,0) != 0x4d615363)) {
                if ((snd_fp.ReadUbyteFromFile( header3) == 4)
                        && (read_l(header3, 0) == 0x4D615364))
                {
                    byte[] buf = new byte[8];
                    ushort numSounds;
                    long snd_fp_seek;

                    /* Read header size and calculate num of sounds */
                    snd_fp.ReadUbyteFromFile( buf, 2);
                    numSounds = (ushort)(read_w(buf, 0) / 18); // 18= size of
                                                               // header
                                                               // element
                    if (LOGSND)
                    {
                        log("logsnd", "Music scores: " + numSounds);
                    }
                    for (i = 0; i < numSounds; i++)
                    {
                        JMagneticSound newSound = new JMagneticSound();
                        if (LOGSND)
                        {
                            log("logsnd", "Sound no. " + i);
                        }
                        /* Read name of element */
                        snd_fp.ReadUbyteFromFile( buf, 8);
                        newSound.name = (Encoding.ASCII.GetString(buf.TakeWhile(p => p != 0).ToArray())).trim();
                        if (LOGSND)
                        {
                            log("logsnd", "Music name: "
                                    + newSound.name);
                        }

                        /* Read tempo */
                        snd_fp.ReadUbyteFromFile( buf, 2);
                        newSound.tempo = read_w(buf, 0);
                        if (LOGSND)
                        {
                            log("logsnd", "Recommended tempo: "
                                    + newSound.tempo);
                        }

                        /* Read file offset */
                        snd_fp.ReadUbyteFromFile( buf, 4);
                        s_offset = read_l(buf, 0);
                        if (LOGSND)
                        {
                            log("logsnd", "Sound offset: " + s_offset);
                        }

                        /* Read file length */
                        snd_fp.ReadUbyteFromFile( buf, 4);
                        s_length = read_l(buf, 0);
                        if (LOGSND)
                        {
                            log("logsnd", "Sound length: " + s_length);
                        }

                        snd_fp_seek = snd_fp.getFilePointer();
                        snd_fp.seek(s_offset);
                        newSound.data = new byte[s_length];
                        snd_fp.ReadUbyteFromFile( newSound.data,
                                s_length);
                        snd_fp.seek(snd_fp_seek);

                        this.sounds.Add(newSound.name, newSound);
                    }
                }
                snd_fp.close();
            }

        }

        uint read_l(byte[] data, uint off)
        {

            uint tmp1 = (uint)data[off] << 24;
            uint tmp2 = (uint)data[off + 1] << 16;
            uint tmp3 = (uint)data[off + 2] << 8;
            uint tmp4 = (uint)data[off + 3];
            uint res = (uint)(tmp1 | tmp2 | tmp3 | tmp4);
            return res;
        }

        ushort read_w(byte[] data, uint off)
        {
            ushort res;
            res = (ushort)(data[off] << 8 | data[off + 1]);
            return res;
        }

        void log(String cat, String s)
        {
            if (cat.startsWith("logsnd"))
            {
                if (sndLogger == null)
                    sndLogger = Logger.getLogger("logsnd");
                sndLogger.info(s);
            }
            else
            {
                Logger.getRootLogger().warn(s);
            }
            // Console.WriteLine(s);
        }

        public JMagneticSound GetSound(string name)
        {
            return this.sounds[name];
        }
    }
}
